#!/bin/sh

wget http://www.omnetpp.org/omnetpp/doc_download/2245-omnet-422-source--ide-tgz
tar xzvf omnetpp-4.2.2-src.tgz
cd omnetpp-4.2.2
NO_TCL=yes ./configure
export PATH=$PATH:/home/s2364307/sim/omnetpp-4.2.2/bin
make -j6 MODE=release

cd ..
wget http://omnetpp.org/download/contrib/models/inet-20111118-src.tgz
tar xzvf inet-20111118-src.tgz
cd inet
make makefiles
make -j6 MODE=release


