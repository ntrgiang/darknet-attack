#include <AppCommon.h>
#include <iostream>

using std::cout;


std::string typeToString(DarknetMessage *msg) {
    switch (msg->getType()) {
    case DM_UNKNOWN:
        return std::string("DM_UNKNOWN");
    case DM_REQUEST:
        return std::string("DM_REQUEST");
    case DM_RESPONSE:
        return std::string("DM_RESPONSE");
    case DM_CON_SYN:
        return std::string("DM_CON_SYN_");
    case DM_CON_ACK:
        return std::string("DM_CON_ACK");
    case DM_FORWARD:
        return std::string("DM_FORWARD");
    case DM_HELLO:
        return std::string("DM_HELLO");
    case DM_GOODBY:
        return std::string("DM_GOODBY");
    case DM_ROUTINGID:
        return std::string("DM_ROUTINGID");
    case DM_ROUTINGID_REQ:
        return std::string("DM_ROUTINGID_REQ");
    case DM_ROUTINGID_DROP:
        return std::string("DM_ROUTINGID_DROP");
    case DM_ROUTING:
        return std::string("DM_ROUTING");
    case DM_ROUTING_RESPONSE:
        return std::string("DM_ROUTING_RESPONSE");
    case DM_STORAGE:
        return std::string("DM_STORAGE");
    case DM_LOOKUP:
        return std::string("DM_LOOKUP");
    default:
        return std::string("!! UNKNOWN MSG TYPE");
    }
}

