//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef MONITOR_H_
#define MONITOR_H_

#include "IDs.h"
#include <omnetpp.h>
#include <fstream>
#include <map>
#include <vector>
#include <queue>
#include <string>
#include <time.h>

#include "StringTreeNodeID.h"

using std::map;
using std::vector;
using std::string;
using std::cout;
using std::endl;

//normally files are just strings. This struct is only here to hold meta info necessary for testing (like when was the content created and stored in the network)
struct StoredFile {
    std::string file;
    RoutingIDPtr fileID;
    simtime_t creationTime;

    StoredFile(std::string &file, RoutingIDPtr fileID) : file(file), fileID(fileID) {
        creationTime = simTime();
    }
};

//this class can be used to remember global information used for testing.
//Currently it stores every content that was stored in the network.
//Then a peer can randomly select one of those and perform a lookup for its ID.
//also This class holds variables for counting how many Lookups were successful, failed, or timed out.
//
//You could also extend this class to store RoutingIDs currently used by nodes, if you wanted to test lookup of nodes instead of content
//(but you would have to implement node lookup first ;) )
class Monitor {
public:

    cModule *peer;

    time_t startTime;

    std::vector<StoredFile> storedFiles;

    static Monitor instance;

    int idLookup;
    int idLookupSuccess;
    int incorrectIdRouting;

    int success;
    int failure;
    int timeout;
    int dropByChurn;

    int noCloserNeighborFailure;
    int noNextHopFailure;
    int badResponseFailure;

    // To keep track of the IDs of all hosts, in all trees
    map<string, map<unsigned int, RoutingIDPtr> > m_routingIdList;

    // To keep track of the joining time of peers
    vector<double> m_joinTimes;
    double m_maxJoinTime;

    // Hop count
    vector<int> m_hopCounts;

    // Root change
    map<int, double> m_rootChanges;

    // -- Connected components
    map<string, vector<string> > m_friendList;
    vector<string> m_offlineNodes;
    vector<string> m_onlineNodes;
    map<int, vector<string> > m_connectedComponents;

    Monitor() :
        m_maxJoinTime(-1.0)
    {
        m_hopCounts.clear();
        m_rootChanges.clear();

        m_friendList.clear();
        m_offlineNodes.clear();
        m_onlineNodes.clear();
        m_connectedComponents.clear();

        m_numSameComponent = 0;
        m_numDiffComponent = 0;
        m_numFailureFromStart = 0L;
    }
    virtual ~Monitor(){}

    void initialize(cModule *peer) {
        this->peer = peer;
        startTime = time(0);
        idLookup = idLookupSuccess = 0;
        incorrectIdRouting = 0;
        dropByChurn = 0;
        success = failure = timeout = 0;
        noCloserNeighborFailure = noNextHopFailure = badResponseFailure = 0;
        storedFiles.clear();

        //m_activeHosts.clear();

        // -- Debug
        //printFriendList();
    }

    //prints result
    void finish() {
        time_t endTime = time(0);

        std::ofstream output(peer->par("testResultFileName"), std::ios_base::app);

        //print network name
        output << "Simulation of " << simulation.getSystemModule()->getName() << endl;

        //print start time (and date)
        tm *timeStruct = localtime(&startTime);
        char buffer[100];
        strftime(buffer, 100, "%H:%M:%S %m/%d/%y", timeStruct);
        output << "BEGIN AT: " << buffer << endl;

        //print duration
        output << "DURATION: ";
        printTimeDiff(output, startTime, endTime);

        //print result
        int total = success + failure + timeout;
        output << "RESULT: total: " << total << ", success: " << success << ", failure: " << failure << ", timeout: " << timeout << endl;
        output << endl;
    }

    virtual void printTimeDiff(std::ofstream &output, time_t beginning, time_t end) {
        unsigned long long seconds = (unsigned long long)difftime(end, beginning);
        unsigned hours = seconds / 3600;
        seconds %= 3600;
        unsigned minutes = seconds / 60;
        seconds %= 60;
        output << hours << ":" << (minutes < 10 ? "0" : "") << minutes << ":" << (seconds < 10 ? "0" : "") << seconds << endl;
    }

    // -- Host related
    //
    RoutingIDPtr getRandomNodeId(unsigned int tree = 0);
    vector<RoutingIDPtr> getAllRoutingIdPtr(string nodeID);
    string findNodeId(RoutingIDPtr rId);
    void storeOneRoutingId(const string hostname, unsigned int tree, const RoutingIDPtr id);
    void printRoutingIdList();
    void reportHopCount(int hopCount);
    void reportJoinTime(double joinTime);
    double getMaxJoinTime();
    void reportRootChange(int treeID, double time);

    // -- Connected components
    void addFriendList(string host, vector<string> friends);
    void reportOnlineNode(string nodeID);
    void reportOfflineNode(string nodeID);
    void findConnectdComponents();
    int getComponentNumber(string nodeID);
    bool inSameComponent(string localNodeID, string remoteNodeID);

    void printFriendList();
    void printOnlineNodes();
    void printOfflineNodes();
    void printConnectedComponents();

    void incrementNumSameComponent()    { m_numSameComponent++; }
    void incrementNumDiffComponent()    { m_numDiffComponent++; }
    void incrementNumFailureFromStart() { m_numFailureFromStart++; }
    inline int getNumSameComponent()        { return m_numSameComponent; }
    inline int getNumDiffComponent()        { return m_numDiffComponent; }
    inline long getnumFailureFromStart()    { return m_numFailureFromStart; }

    // -- For Debugging
    void addLookupMsg(long msgTreeId);
    void addIncorrectRoutingMsg(long msgTreeId);
    void addSuccessMsg(long msgTreeId);
    vector<long> getLookupMessages();
    vector<long> getIncorrectRoutingMessages();
    vector<long> getSuccessMessages();
    void printLookupMessages();
    void printIncorrectRoutingMessages();
    void printSuccessMessages();

protected:
    bool nodeIsOnline(string nodeID);
    bool inList(vector<string> l, string nodeID);
    void deleteNodeFromList(vector<string> &list, string nodeID);
    void printList(vector<string> l);
    void printQueue(std::queue<string> q);

protected:
    int m_numSameComponent;
    int m_numDiffComponent;
    long m_numFailureFromStart;

    // -- For debugging
    vector<long> m_lookupMessages;
    vector<long> m_incorrectRoutingMessages;
    vector<long> m_successMessages;
};

#endif /* MONITOR_H_ */
