/*
 * OffsetGenerator.h
 *
 *  Created on: 06.06.2013
 *      Author: Matthias
 */

#ifndef OFFSETGENERATOR_H_
#define OFFSETGENERATOR_H_

#include "IDs.h"
#include "PrefixRoutingID.h"
#include <vector>


class OffsetGenerator {
public:
    virtual ~OffsetGenerator(){}
    //neighbor must not be NULL
    virtual RoutingIDPtr request(TreeNodeIDPtr neighbor) = 0;

    //if neighbor is not known (or NULL) a NULL Pointer is returned
    virtual RoutingIDPtr get(TreeNodeIDPtr neighbor) = 0;

    virtual bool moreAvailable() = 0;
    virtual void drop(TreeNodeIDPtr neighbor) = 0;
    virtual void clear() = 0;
};


struct OffsetLease {
    TreeNodeIDPtr neighbor;
    RoutingIDPtr offset;
};

class SimpleOffsetGenerator : public OffsetGenerator {

std::vector<OffsetLease> offsetLeases;

public:
    virtual ~SimpleOffsetGenerator(){}

    virtual RoutingIDPtr request(TreeNodeIDPtr neighbor) {

        //first check if we have a lease for neighbor already
        RoutingIDPtr existingOffset = get(neighbor);
        if (existingOffset)
            return existingOffset;

        //if no lease was found we will now search for an empty position in offsetLeases
        unsigned position = 0;
        for (; position < offsetLeases.size(); position++) {
            if (!offsetLeases[position].neighbor) {
                break;
            }
        }
        //notice that this is the only place in this class where knowledge of the class Prefix is required.
        //OffsetLease newEntry{neighbor, RoutingID::create(position)};
        OffsetLease newEntry;
            newEntry.neighbor = neighbor;
            newEntry.offset = RoutingID::create(position);
        if (position == offsetLeases.size())
            offsetLeases.push_back(newEntry);
        else
            offsetLeases[position] = newEntry;

        return newEntry.offset;
    }

    virtual RoutingIDPtr get(TreeNodeIDPtr neighbor) {
        if (neighbor) {
            for (unsigned i = 0; i < offsetLeases.size(); i++) {
                if (offsetLeases[i].neighbor == neighbor) {
                    return offsetLeases[i].offset;
                }
            }
        }
        return RoutingIDPtr();
    }

    virtual bool moreAvailable() {
        return true;
    }

    virtual void drop(TreeNodeIDPtr neighbor) {
        for (unsigned i = 0; i < offsetLeases.size(); i++) {
            if (offsetLeases[i].neighbor == neighbor) {
                offsetLeases[i].neighbor.reset();
                offsetLeases[i].offset.reset();
            }
        }
    }

    //deletes all leases older than the given argument. most importantly if the argument is < 0, all leases will be deleted.
    virtual void clear() {
        offsetLeases.clear();
    }

};



#endif /* OFFSETGENERATOR_H_ */
