/*
 * IDs.cc
 *
 *  Created on: 25.06.2013
 *      Author: Matthias
 */

#include "IDs.h"
#include "PrefixRoutingID.h"


RoutingIDPtr RoutingID::create() {
    RoutingIDPtr result(new PrefixRoutingID());
    return result;
}

RoutingIDPtr RoutingID::create(int offsetIndex) {
    RoutingIDPtr result(new PrefixRoutingID(offsetIndex));
    return result;
}
