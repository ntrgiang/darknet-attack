//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TreeNode.h"
#include "Peer.h"
#include "logging.h"
#include <iomanip>


//#define TKENV_
#ifdef TKENV_
   #define debugOUT EV
#else
   #define debugOUT (!false) ? std::cout : std::cout \
   << "@" << std::setprecision(8) << simTime().dbl() \
   << "::TreeNode: "
#endif


TreeNode::TreeNode(Peer &peer, TreeNodeOrganizer &treeNodeOrganizer, TreeNodeIDPtr treeNodeID, int treeID) :
peer(peer), treeNodeOrganizer(treeNodeOrganizer), treeNodeID(treeNodeID), treeID(treeID){
    offsetGenerator = new SimpleOffsetGenerator;
    minStableRoutingIDPeriod = peer.par("minStableRoutingIDPeriod").doubleValue();
    maxRootDistance = peer.par("maxRootDistance");
    helloInterval = peer.par("helloInterval");
    updateInterval = peer.par("updateInterval");
#ifdef LOGGING
    getLog(treeID) << peer.getNodeID() << " = " << treeNodeID->toString() << endl;
#endif
}

TreeNode::~TreeNode() {
    delete offsetGenerator;
}

void TreeNode::activated() {
    //create initial state:
    clearState();

    generateUpdateID();

    //NeighborInfo for all unknown neighbors (acts as a dummy)
    neighbors.clear();
    NeighborInfo defaultNeighbor;
    //but we also set some other values, to make sure this dummy never does anything important (like becoming parent, requesting a prefix, etc.)
    //also some of these values are important when creating a hello for a new neighbor (if the neighbor is unknown, the dummy will be used as basis for creating the hello)
    //while creating the hello some values from the last hello received from that neighbor will be used. We must be careful while setting the values here!
    defaultNeighbor.neighborID = "";
    defaultNeighbor.nextHelloCounter = 0; //make sure hellos are sent after activation
    defaultNeighbor.hello.parentRequest = false; //if this was true, on creating a hello for a new neighbor we would incorrectly assume that that neighbor
    //wanted to be our child and we would deny it the right to be our child, since we have (of course) no prefix generated for it.
    defaultNeighbor.hello.noMoreChildren = true; //make sure this dummy is never chosen as parent
    neighbors.push_back(defaultNeighbor);

    parentChanged = false;

}

void TreeNode::deactivated() {
    clearState();
}

void TreeNode::clearState() {
    parent.reset();
    pendingParent.reset();
    currentRoot.reset();
    currentParentRoutingID.reset();
    currentOffset.reset();
    currentlyUsedRoutingID.reset();

    currentUpdateID = 0;
    lastParentUpdateID = 0;

    neighbors.clear();
    offsetGenerator->clear();
}


void TreeNode::goodbyReceived(string peerID) {
    //find corresponding neighborInfo object, call neighborDropped() and then delete it.
    for (vector<NeighborInfo>::iterator iter = (neighbors.begin() + 1); iter != neighbors.end(); ++iter) {
        NeighborInfo &neighbor = *iter;
        if (neighbor.neighborID == peerID) {
            neighborDropped(neighbor);
            neighbors.erase(iter);
            break;
        }
    }
}

void TreeNode::update(map<string, HelloInfoContainer> &helloContainers) {
    //delete all old or outdated neighborInfo objects
    //just call neighborDropped() and erase the object.
    for (vector<NeighborInfo>::iterator iter = (neighbors.begin() + 1); iter != neighbors.end(); /*----*/){
        NeighborInfo &neighbor = *iter;
        if (neighbor.age.isOutdated() || neighbor.hello.age.isOutdated()) {
            neighborDropped(neighbor);
            iter = neighbors.erase(iter);
        } else {
            ++iter;
        }
    }

    //may actually change parent to 0, if all other neighbors are worse then this node
    //normally only a pendingParent will be chosen though
    checkForBestParent();

    bool helloToAll = false;

    if (parent) {
        NeighborInfo &parentInfo = getNeighborFromTreeNodeID(parent);
        if (!parentChanged)
            helloToAll = checkStatus(parentInfo);
        updateStatus(parentInfo);
    }

    //check currentlyUsedRoutingIDChange
    if (!currentlyUsedRoutingID || simTime().dbl() - lastRoutingIDChange.dbl() > minStableRoutingIDPeriod) {
        RoutingIDPtr currentRoutingID = getFullRoutingID();
        if (currentRoutingID) {
            currentlyUsedRoutingID = currentRoutingID;
            treeNodeOrganizer.routingComponent->setOwnRoutingID(treeID, currentlyUsedRoutingID);
            Monitor::instance.storeOneRoutingId(peer.getNodeID(), treeID, currentlyUsedRoutingID);
        }
    }

    if (parentChanged) {
        generateUpdateID();
        helloToAll = true;
    }

    if (helloToAll)
        printStatus();

    //fu:r jeden key, get neighborInfo. Wenn update noetig, erstelle hello und fu:ge es ein.
    //falls neighborInfo nicht der dummy ist und helloToAll, mache das gleiche.
    //ausserdem: counter verringern, wenn kein hello, bzw. neu setzen, wenn hello

    for (unsigned i = 0; i < neighbors.size(); i++)
        neighbors[i].nextHelloCounter -= updateInterval;

    for (map<string, HelloInfoContainer>::iterator iter = helloContainers.begin(); iter != helloContainers.end(); ++iter) {
        HelloInfoContainer &container = iter->second;
        NeighborInfo &neighbor = getNeighborFromPeerID(iter->first);
        if (neighbor.nextHelloCounter <= 0 || (!neighbor.neighborID.empty() && helloToAll)) {
            container.add(getHelloInfoForNeighbor(iter->first));
#ifdef LOGGING
            getLog(treeID) << peer.getNodeID() << " sends hello to " << iter->first << endl;
#endif
            //reset timer for next hello, unless neighbor is the dummy for the not-yet-known neighbors
            //(because that dummy might be used for several neighbors and we don't want to reset the timer until the hello for the last of those was created)
            if (!neighbor.neighborID.empty())
                neighbor.nextHelloCounter = helloInterval;
        }
    }

    if (neighbors[0].nextHelloCounter <= 0)
        neighbors[0].nextHelloCounter = helloInterval;

    parentChanged = false;
}

void TreeNode::checkForBestParent() {
    //first get bestParent (or NULL, if no parent is better then this peer and this peer should be root)
    TreeNodeIDPtr bestParent;

    NeighborInfo *bestParentInfo;
    for (unsigned i = 1; i < neighbors.size(); i++) {
        NeighborInfo &neighbor = neighbors[i];
        if (!canBeParent(neighbor))
            continue;

        if (!bestParent) {
            if (neighbor.hello.root->compareTo(treeNodeID) < 0)
                bestParent = neighbor.hello.sender;
        } else {
            int compResult = neighbor.hello.root->compareTo(bestParentInfo->hello.root);
            if (compResult < 0) {
                bestParent = neighbor.hello.sender;
            } else if (compResult == 0) {
                if (neighbor.hello.rootDist < bestParentInfo->hello.rootDist) {
                    bestParent = neighbor.hello.sender;
                } else if (neighbor.hello.rootDist == bestParentInfo->hello.rootDist) {
                    if (neighbor.hello.sender->compareTo(bestParentInfo->hello.sender) < 0) {
                        bestParent = neighbor.hello.sender;
                    }
                }
            }
        }

        //if the current neighbor would be a better parent, we must remember the NeighborInfo object, to compare it to the next neighbors
        if (bestParent == neighbor.hello.sender)
            bestParentInfo = &neighbor;
    }

    //if no neighbor is a good parent and it would be to have this peer as root, then we immediately make it so (we don't have to send a request to ourselves)
    //of course that is only if we weren't root already
    if (!bestParent && parent) {
        dropParent();

        if (!parent) {
            // change from being a non-root to root
            Monitor::instance.reportRootChange(treeID, simTime().dbl());
        }
    }

    //if a new best parent was found, set pending parent and make sure, a hello (with the parent request) is sent immediately
    //otherwise there won't be a pending parent;
    if (bestParent && bestParent != parent) {
        pendingParent = bestParent;
        bestParentInfo->nextHelloCounter = 0;
    } else if (pendingParent) {
        dropPendingParent();
    }

#ifdef LOGGING
    if (pendingParent)
        getLog(treeID) << treeNodeID->toString() << " selected a pending parent: " << pendingParent->toString() << endl;
#endif
}

void TreeNode::updateStatus(NeighborInfo &parentInfo) {
    currentRoot = parentInfo.hello.root;
    currentRootDist = parentInfo.hello.rootDist + 1;
    if (lastParentUpdateID != parentInfo.hello.updateID)
        lastParentUpdateID = currentUpdateID = parentInfo.hello.updateID;
    currentParentRoutingID = parentInfo.hello.routingID;
    currentOffset = parentInfo.hello.offset;

    //set our own routingID in routing component. This has nothing to do with the actual tree building and is only required for routing
    treeNodeOrganizer.routingComponent->setOwnRoutingID(treeID, getFullRoutingID());
    Monitor::instance.storeOneRoutingId(peer.getNodeID(), treeID, currentlyUsedRoutingID);
}

bool TreeNode::checkStatus(NeighborInfo & parentInfo) {
    if (parentInfo.hello.root != currentRoot) {
        return true;
    }

    if (parentInfo.hello.rootDist + 1 != currentRootDist) {
        return true;
    }

    if (parentInfo.hello.updateID != lastParentUpdateID) {
        return true;
    }

    RoutingIDPtr newFullRoutingID;
    if (parentInfo.hello.routingID && parentInfo.hello.offset)
        newFullRoutingID = parentInfo.hello.routingID->append(parentInfo.hello.offset);
    if (!RoutingID::equal(newFullRoutingID, getFullRoutingID())) {
        return true;
    }

    return false;
}

void TreeNode::checkForCircle(HelloInfo &hello, NeighborInfo &neighbor) {
    //debugOUT << "___checkForCircle___" << endl;

    //if the neighbor is blocked, check if we can unblock it
    if (neighbor.circleDetected) {
        //unblock neighbor, if it sent a new updateID
        if (hello.updateID != neighbor.hello.updateID) {
            neighbor.circleDetected = false;
#ifdef LOGGING
            getLog(treeID) << treeNodeID->toString() << " unblocked " << hello.sender->toString() << "!" << endl;
#endif
        }
    }
    //check for circle
    //we will block a neighbor (i.e. not allow it to be parent) if the hello contains our currentUpdateID AND that ID was created by us
    //(intentionally not else if)
    if (neighbor.circleDetected == false && hello.updateID == currentUpdateID && currentUpdateID != lastParentUpdateID) {
        neighbor.circleDetected = true;
#ifdef LOGGING
        getLog(treeID) << treeNodeID->toString() << " blocked " << hello.sender->toString() << " because a circle was detected! updateID=" << currentUpdateID << endl;
#endif
    }
}

void TreeNode::checkForIDChange(HelloInfo &hello, NeighborInfo &neighbor) {
    //debugOUT << "___checkForIDChange___" << endl;

    //notice how comparison of treeNodeID pointers is still correct for comparing treeNodeIDs. Even if those IDs change during the simulation, there must always be only one instance for one treeNodeID!
    if (hello.sender != neighbor.hello.sender) {
        neighborDropped(neighbor);
        neighbor = createNewNeighborInfo(neighbor.neighborID, hello);
    }
}

bool TreeNode::canBeParent(NeighborInfo &neighbor) {
    return !neighbor.circleDetected && !neighbor.hello.noMoreChildren && neighbor.hello.rootDist < maxRootDistance && !offsetGenerator->get(neighbor.hello.sender) && !neighbor.hello.age.isOutdated();

}

void TreeNode::helloReceived(HelloInfo hello, string peerID) {
#ifdef LOGGING
    getLog(treeID) << peer.getNodeID() << " receives hello from " << peerID << endl;
#endif
//TODO: remove! this is only for debugging to see if any cycles are created
    if (hello.rootDist > maxRootDistance)
        getLog(0) << peer.getNodeID() << " too big rootDist" << endl;

    //step 0: update the currently used routingID in the routingComponent, so routing to the sender of this hello can be done correctly
    //this has nothing to do with the actual tree building!
    treeNodeOrganizer.routingComponent->addRoutingTableEntry(peerID, treeID, hello.currentlyUsedRoutingID);

    //step 1: get neighborInfo. Create new one, if none exists for the sender (i.e. the get-method returns the dummy instance)
    NeighborInfo *neighborInfo = &getNeighborFromPeerID(peerID);
    if (neighborInfo->neighborID.empty()) {
        neighbors.push_back(createNewNeighborInfo(peerID, hello));
        neighborInfo = &neighbors.back();
    }

    //step 2: check for special events, like if a circle was detected or if the TreeID of the neighbor changed.
    checkForCircle(hello, *neighborInfo);
    checkForIDChange(hello, *neighborInfo);

    //step 3: update hello
    neighborInfo->age.set(0);
    neighborInfo->hello = hello;

    //step 4a: if the sender is the parent, check if still stays parent, otherwise this peer becomes root (for now).
    if (parent && parent == hello.sender) {
        if (!canBeParent(*neighborInfo) || !hello.offset) {
            dropParent();
        }
        //if the parent does not drop us, we could also update the status here, or even check, if the parent is still the best possible parent.
        //but I think it would not be helpful and make things more complicated, since I think we only need to update the status right before we send out hellos, which we only do in update()
    }
    //step 4b: if the sender is the pendingParent, check if it confirmed or rejected our parentRequest.
    else if(pendingParent && pendingParent == hello.sender) {
        if (!canBeParent(*neighborInfo)) {
            dropPendingParent();
        } else if (hello.offset) {
            changeParent();
        }
    }

    //step 5: check if the sender asks for an offset (i.e. asks us to become its parent)
    if (hello.parentRequest) {
        RoutingIDPtr offsetBefore = offsetGenerator->get(hello.sender);

        offsetGenerator->request(hello.sender);

        if (!offsetBefore || !RoutingID::equal(offsetBefore, offsetGenerator->get(hello.sender))) {
            //if no previous offset was generated for the sender, we answer immediately. Otherwise we only answer immediately if the offset changed.
            neighborInfo->nextHelloCounter = 0;
        }
    } else {
        offsetGenerator->drop(hello.sender);
    }
}

void TreeNode::neighborDropped(NeighborInfo &neighborInfo) {
    if (parent && neighborInfo.hello.sender == parent) {
        dropParent();
    } else if (pendingParent && neighborInfo.hello.sender == pendingParent) {
        dropPendingParent();
    }
    dropChild(neighborInfo.hello.sender);
    treeNodeOrganizer.routingComponent->addRoutingTableEntry(neighborInfo.neighborID, treeID, RoutingIDPtr());
}


void TreeNode::dropParent() {
    parent.reset();
    parentChanged = true;
#ifdef LOGGING
    getLog(treeID) << treeNodeID->toString() << " dropped parent and is root now" << endl;
#endif

}

void TreeNode::changeParent() {
    parent = pendingParent;
    parentChanged = true;
    pendingParent.reset();

#ifdef LOGGING
    getLog(treeID) << treeNodeID->toString() << " changed parent to " << parent->toString() << endl;
#endif
}

void TreeNode::dropPendingParent() {
    pendingParent.reset();
#ifdef LOGGING
    getLog(treeID) << treeNodeID->toString() << " dropped its pending parent" << endl;
#endif
}

void TreeNode::dropChild(TreeNodeIDPtr childID) {
    offsetGenerator->drop(childID);
}

HelloInfo TreeNode::getHelloInfoForNeighbor(const string &peerID) {
    HelloInfo hello;

    NeighborInfo &neighborInfo = getNeighborFromPeerID(peerID);
    HelloInfo &neighborHello = neighborInfo.hello;

    TreeNodeIDPtr receiver = neighborHello.sender;

    hello.sender = treeNodeID;
    hello.treeID = treeID;
    hello.routingID = getFullRoutingID();

    hello.offset = offsetGenerator->get(receiver);

    if (!parent) {
        //we are root
        hello.root = treeNodeID;
        hello.rootDist = 0;
        hello.age.set(0);
        hello.age.maxAge = peer.par("maxHelloAge");
    } else {
        //we are not root
        HelloInfo &helloFromParent = getNeighborFromTreeNodeID(parent).hello;
        hello.root = helloFromParent.root;
        hello.rootDist = helloFromParent.rootDist + 1;
        hello.age = helloFromParent.age;
    }

    if (receiver && (receiver == parent || receiver == pendingParent))
        hello.parentRequest = true;
    else
        hello.parentRequest = false;

    hello.noMoreChildren = false;
    if (neighborHello.parentRequest) {
        //if the neighbor wanted to be our parent, we must check if we granted an offset
        //if not, we set noMoreChildren to true
        if (!hello.offset)
            hello.noMoreChildren = true;
    } else {
        //if there was no request, we set noMoreChildren to false only if we have the maximum number of children
        if (!offsetGenerator->moreAvailable())
            hello.noMoreChildren = true;
    }

    hello.updateID = currentUpdateID;

    hello.currentlyUsedRoutingID = currentlyUsedRoutingID;

    //we assume that the returned hello will also be sent. We mark the neighborInfo object by reducing the counter
    neighborInfo.nextHelloCounter--;

    return hello;
}

NeighborInfo TreeNode::createNewNeighborInfo(const string &neighborID, const HelloInfo &hello) {
    NeighborInfo result;
    result.hello = hello;
    result.neighborID = neighborID;
    result.nextHelloCounter = 0;
    result.circleDetected = false;
    result.age.maxAge = peer.par("neighborTimeout");
    return result;
}

NeighborInfo &TreeNode::getNeighborFromPeerID(const string &peerID) {
    for (unsigned i = 1; i < neighbors.size(); i++)
        if (neighbors[i].neighborID == peerID)
            return neighbors[i];

    return neighbors[0];
}

NeighborInfo &TreeNode::getNeighborFromTreeNodeID(TreeNodeIDPtr treeNodeID) {
    for (unsigned i = 1; i < neighbors.size(); i++)
        if (neighbors[i].hello.sender == treeNodeID)
            return neighbors[i];

    return neighbors[0];
}

RoutingIDPtr TreeNode::getFullRoutingID() {
    if (!parent)
        return RoutingID::create(); //if we are root, return empty instance

    if (!currentParentRoutingID || !currentOffset)
        return RoutingIDPtr();

    return currentParentRoutingID->append(currentOffset);
}

void TreeNode::generateUpdateID() {
    do {
        currentUpdateID = peer.getRNG(0)->intRand();
    } while(currentUpdateID == 0);
}


void TreeNode::printStatus() {
#ifdef LOGGING
    getLog(treeID) << treeNodeID->toString() << " status: ";
    if (parent == 0) {
        getLog(treeID, false) << "ROOT" << endl;
    } else {
        getLog(treeID, false) << "parent=" << parent->toString() << ", root=" << (currentRoot != 0 ? currentRoot->toString() : "NULL")
                << ", distance=" << currentRootDist << ", parentRoutingID=" << (currentParentRoutingID ? currentParentRoutingID->toString() : "NULL")
                << ", offset=" << (currentOffset ? currentOffset->toString() : "NULL") << ", updateID=" << currentUpdateID << endl;
    }
#endif
}
