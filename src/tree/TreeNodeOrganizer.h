//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TREENODEORGANIZER_H_
#define TREENODEORGANIZER_H_

#include "IDs.h"
#include "EventHandler.h"
#include <omnetpp.h>
#include "messages/HelloMessage_m.h"
#include "TreeNode.h"
#include <vector>
#include "RoutingComponent.h"

class Peer;


using namespace std;


class TreeNodeOrganizer : EventHandler {

    double updateInterval;

    cMessage *periodicalEvent;
    Peer &peer;
    vector<TreeNode *> treeNodes;


public:
    RoutingComponent *routingComponent;

    TreeNodeOrganizer(Peer &peer);
    virtual ~TreeNodeOrganizer();

    virtual void messageReceived(DarknetMessage* msg);
    virtual void helloReceived(HelloMessage* msg);
    virtual void goodbyReceived(DarknetMessage *msg);

    virtual void handleActivation();
    virtual void handleDeactivation();

    virtual void handleEvent(cMessage *event);

    virtual void collectAndSendHellos();
    // -- New
    void printIdAllTrees();

};

#endif /* TREENODEORGANIZER_H_ */
