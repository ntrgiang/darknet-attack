//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "RoutingComponent.h"
#include "Peer.h"
#include <assert.h>
#include <iomanip>
#include "logging.h"

using namespace std;
using namespace boost;


//#define TKENV_
#ifdef TKENV_
   #define debugOUT EV
#else
   #define debugOUT (!false) ? std::cout : std::cout \
   << "@" << std::setprecision(8) << simTime().dbl() \
   << "::RoutingComponent: "
#endif


RoutingComponent::RoutingComponent(Peer &peer, int numberOfTrees) : peer(peer), numberOfTrees(numberOfTrees) {
    routingTables.resize(numberOfTrees);
    ownRoutingIDs.resize(numberOfTrees);
    routingInfoChanged.resize(numberOfTrees);
    routingTimeOut = peer.par("routingTimeOut").doubleValue();
    redistributionInterval = peer.par("redistributionInterval");
    contentIDLength = peer.par("contentIDLength");
    contentIDRange = peer.par("contentIDRange");
    minNeighborStableTime = peer.par("storage_minNeighborStableTime").doubleValue();
    advancedRoutingStrategy = peer.par("advancedRoutingStrategy").boolValue();
}

RoutingComponent::~RoutingComponent() {

    for (unordered_map<RoutingIDPtr, Content*>::iterator iter = content.begin(); iter != content.end(); ++iter) {
        delete iter->second;
    }
}

void RoutingComponent::clear() {
    for (int i = 0; i < numberOfTrees; i++) {
        routingTables[i].clear();
        ownRoutingIDs[i].reset();
        routingInfoChanged[i] = false;
    }
    forwardingInfo.clear();
}


void RoutingComponent::activated() {
    //debugOUT << "___activated___" << endl;

    clear();
    //take responsibility for every content in every tree (that way it will be redistributed, as soon as a stable better parent is found)
    for (unordered_map<RoutingIDPtr, Content*>::iterator iter = content.begin(); iter != content.end(); ++iter) {
        for (int t = 0; t < numberOfTrees; t++) {
            iter->second->redistributionInfo[t].initAfterActivation();
        }
    }
}

void RoutingComponent::deactivated() {
    //signal timeout for all pending routing requests
    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin(); iter != forwardingInfo.end(); ++iter) {
        if (iter->callback)
            iter->callback->routingTimeout(iter->target);
    }

    clear();
}

void RoutingComponent::update() {
    //debugOUT << "___update___" << endl;

    //remove old forwardingInfos
    //if they were not used for routing a response back, handle timeout, depending on type
    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin(); iter != forwardingInfo.end();) {
        ForwardingInfo &fInfo = *iter;
        bool deleteEntry = false;

        if (fInfo.creationTime + routingTimeOut <= simTime()) { //timeout
            //debugOUT << "timeout" << endl;
            deleteEntry = true;
            if (!fInfo.forwarded && fInfo.sender.empty()) { //fInfo not handled yet
                //debugOUT << "fInfo not handled yet" << endl;
                if (fInfo.type == DM_STORAGE) {
                    //find the corresponding content and call timeout
                    if (content.find(fInfo.target) != content.end()) {
                        content[fInfo.target]->redistributionInfo[fInfo.treeID].storageRequestTimeout();
                    }
                } else if (fInfo.type == DM_LOOKUP) {
//                  assert(iter->callback);
                    //if we are using the advanced routing strategy and this is the first try (routing only in the best tree)
                    //then we will try routing in the other trees.
                    //otherwise we just call timeout on callback
                    if (advancedRoutingStrategy && fInfo.remainingAttempts == 0) {
                        lookupInOtherTrees(fInfo, true);
                        if (fInfo.remainingAttempts > 0)
                            deleteEntry = false;
                    } else {
                        fInfo.callback->routingTimeout(fInfo.target);
                    }
                } else if (fInfo.type == DM_IDLOOKUP) {
                    //debugOUT << "DM_IDLOOKUP" << endl;
                    //debugOUT << "advancedRoutingStrategy = " << advancedRoutingStrategy << endl;
                    fInfo.callback->routingTimeout(fInfo.target);
                }

            } // fInfo
        } // timeout

        if (deleteEntry)
            iter = forwardingInfo.erase(iter);
        else
            ++iter;
    }

    //since stuff only gets deleted here, we only have to sort the content here.
    //the content will be sorted so that you will probably want to delete the elements at the end first.
    //most importantly all content that is no longer required, because this peer is not responsible for it any more, is put at the very end!
    //sort(content.begin(), content.end());

    //for each content:
    //delete if this peer is no longer responsible for it in any tree
    //set bestNeighbor for each tree
    //start storage request if required (also for each tree)
    //for (vector<Content*>::iterator iter = content.begin(); iter != content.end(); ) {
    for (unordered_map<RoutingIDPtr, Content*>::iterator iter = content.begin(); iter != content.end(); ) {
        Content &c = *iter->second;
        if (c.canBeDeleted()) {
            //we only check if the content is not required anymore, but of course you can also delete elements
            //by a different strategy, like removing all but 100 entries
            //also notice that because of the sorting we did earlier, when we want to delete an element it will always be at the end so we can use pop_back()
            //(unless your strategy does not rely on the sorting, then deleting the elements may be more complicated and maybe also less efficient!)
#ifdef LOGGING
            ROUTING_LOG() << peer.getNodeID() << " completely drops " << c.ID->toString() << "(" << c.file << ")" << endl;
#endif
            delete iter->second;
            iter = content.erase(iter);
            continue;
        }
        for (int treeID = 0; treeID < numberOfTrees; treeID++) {
            if (routingInfoChanged[treeID] || c.newContent) {
                c.redistributionInfo[treeID].setBetterNeighbor(getNextHop(c.ID, treeID));
            }
            if (c.redistributionInfo[treeID].redistributionNecessary()) {
#ifdef LOGGING
                ROUTING_LOG() << peer.getNodeID() << " is redistributing " << c.ID->toString() << "(" << c.file << ") in tree " << treeID << endl;
#endif
                //create and send storage message
                RoutingMessage *msg = new RoutingMessage;
                msg->setType(DM_STORAGE);
                msg->setSrcNodeID(peer.getNodeID().c_str());
                msg->setDestNodeID(c.redistributionInfo[treeID].betterNeighbor.c_str());
                msg->setTreeID(treeID);
                msg->setTarget(c.ID);
                msg->setFile(c.file.c_str());
                msg->setRequestID(generateRequestID());
                peer.sendMessage(msg);
                c.redistributionInfo[treeID].storageRequestStarted();

                ForwardingInfo fInfo(msg);
                fInfo.sender = "";
                forwardingInfo.push_back(fInfo);
            }
        }
        c.newContent = false;
        ++iter;
    }

    for (int t = 0; t < numberOfTrees; t++)
        routingInfoChanged[t] = false;
}

void RoutingComponent::addRoutingTableEntry(string &neighborID, int treeID, RoutingIDPtr routingID) {
    //debugOUT << "___addRoutingTableEntry___" << endl;

    map<string, RoutingIDPtr> &table = routingTables[treeID];
    RoutingIDPtr old;
    if (table.find(neighborID) != table.end())
        old = table[neighborID];

    if (!RoutingID::equal(routingID, old)) {
        if (routingID) {
            routingTables[treeID][neighborID] = routingID;
        } else {
            routingTables[treeID].erase(neighborID);
        }
        routingInfoChanged[treeID] = true;
    }
}

void RoutingComponent::setOwnRoutingID(int treeID, RoutingIDPtr routingID) {
    //debugOUT << "___setOwnRoutingID___" << endl;

    if (!RoutingID::equal(ownRoutingIDs[treeID], routingID)) {
#ifdef LOGGING
        ROUTING_LOG() << peer.getNodeID() << " in tree " << treeID << " now uses routingID " << (routingID ? routingID->toString() : "NULL") << endl;
#endif
        ownRoutingIDs[treeID] = routingID;
        routingInfoChanged[treeID] = true;
    }
}


bool RoutingComponent::hasContent(RoutingIDPtr contentID) {
    return content.find(contentID) != content.end();
}




RoutingIDPtr RoutingComponent::store(string file) {
    //debugOUT << "___store___" << endl;

    //determine ID of the content
    RoutingIDPtr contentID = createIDForContent(file);

#ifdef LOGGING
    ROUTING_LOG() << peer.getNodeID() << " stores " << contentID->toString() << "(" << file << ")" << endl;
#endif

    //check if we already know this content:
    if (content.find(contentID) != content.end())
        return contentID;

    //create a content instance for the new content and store it.
    //the content is not redistributed here, but it will be with the next update. Until then this peer takes responsibility for the content in all trees.
    Content *newContent = new Content(contentID, file, numberOfTrees, true, minNeighborStableTime, redistributionInterval);
    content[contentID] = newContent;

    return contentID;
}

void RoutingComponent::lookup(RoutingIDPtr contentID, RoutingResultCallback *callback) {
    //debugOUT << "___lookup___" << endl;

#ifdef LOGGING
    ROUTING_LOG() << peer.getNodeID() << " wants to lookup " << contentID->toString();
#endif


    //first check, if we have the content (if yes, we immediately call success method on callback and return, without sending any messages)
    if (content.find(contentID) != content.end()) {
#ifdef LOGGING
        ROUTING_LOG(false) << " immediately found!" << endl;
#endif
        callback->routingSuccess(contentID, content[contentID]->file);
        return;
    }

    //find the best neighbors in all tree
    //this is where other routing strategies may be applied
    string nextHop;
    int treeID = getBestTree(contentID, nextHop);

#ifdef LOGGING
    ROUTING_LOG(false) << " in tree " << treeID << endl;
#endif

    //if there is no neighbor closer to contentID than this peer, the routing failed before it even started
    //we call failure method on callback and return without sending any messages
    if (nextHop.empty()) {
        callback->routingFailure(contentID);
        callback->routingNoCloserNeighborFailure(contentID);
        return;
    }

    //create first message and send it to best neighbor
    RoutingMessage *msg = new RoutingMessage;
    msg->setType(DM_LOOKUP);
    msg->setSrcNodeID(peer.getNodeID().c_str());
    msg->setDestNodeID(nextHop.c_str());
    msg->setTreeID(treeID);
    msg->setTarget(contentID);
    msg->setRequestID(generateRequestID());
    peer.sendMessage(msg);

    //create forwardingInfo and also remember callback!
    ForwardingInfo fInfo(msg);
    fInfo.sender = "";
    fInfo.callback = callback;
    forwardingInfo.push_back(fInfo);
}

void RoutingComponent::idLookup(unsigned int treeID, RoutingIDPtr destRoutingID, RoutingResultCallback *callback, bool &lookupSent)
{
//    if (RoutingID::equal(ownRoutingIDs[treeID], destID)) {
//        //cout << "destID " << destID->toString() << " is node's ID itself" << endl;
//        return;
//    }

    assert(!RoutingID::equal(ownRoutingIDs[treeID], destRoutingID));
    string nextHop = getNextHop(destRoutingID, treeID);
    if (nextHop.empty()) {
        Monitor::instance.incrementNumFailureFromStart();
        lookupSent = false;
        return;
    }
    //assert(!nextHop.empty());

    // -- If a best neighbor is found
    // create first message and send it to best neighbor
    RoutingMessage *msg = new RoutingMessage;
    msg->setType(DM_IDLOOKUP);
    msg->setSrcNodeID(peer.getNodeID().c_str());
    msg->setDestNodeID(nextHop.c_str());
    msg->setTreeID(treeID);
    msg->setTarget(destRoutingID);
    msg->setRequestID(generateRequestID());
    msg->setId(msg->getTreeId());
    Monitor::instance.addLookupMsg(msg->getId());
    peer.sendMessage(msg);

    //create forwardingInfo and also remember callback!
    ForwardingInfo fInfo(msg);
    fInfo.sender = "";
    fInfo.callback = callback;
    forwardingInfo.push_back(fInfo);

    lookupSent = true;
    fInfo.callback->routingIdLookup(); // stats


}

void RoutingComponent::handleIdLookup(RoutingMessage *msg)
{
    //cout << "___handleIdLookup___" << endl;

    if (msg->getTTL() == 0) {
        cout << "TTL = 0" << endl;
        return;
    }

    unsigned int treeID = msg->getTreeID();
    //cout << "Routing message in tree " << treeID << endl;

    //cout << "ownRoutingIDs[treeID] = " << ownRoutingIDs[treeID]->toString() << endl;
    //cout << "msg->getTarget() = " << msg->getTarget()->toString() << endl;

    // -- A lookup message arrive at a wrong node!!!
    //
    if (!RoutingID::equal(ownRoutingIDs[treeID], msg->getTarget())) {
        ++Monitor::instance.incorrectIdRouting;
        Monitor::instance.addIncorrectRoutingMsg(msg->getId());
        return;
    }

    //assert(RoutingID::equal(ownRoutingIDs[treeID], msg->getTarget()));

    int hopCount = 255 - msg->getTTL();
    Monitor::instance.reportHopCount(hopCount);
    //cout << "hopCount = " << hopCount << endl;

    RoutingMessage *response = new RoutingMessage;
    response->setType(DM_IDLOOKUP);
    response->setSrcNodeID(peer.getNodeID().c_str());
    response->setDestNodeID(msg->getSrcNodeID());
    response->setTreeID(msg->getTreeID());
    response->setTarget(msg->getTarget());
    response->setRequestID(msg->getRequestID());
    response->setResponse(true);
    response->setId(msg->getId()); // re-use msg's Id for traceability
    peer.sendMessage(response);
}

void RoutingComponent::handleIdLookupResponse(RoutingMessage *msg)
{
    //debugOUT << "___handleIdLookupResponse___" << endl;

    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin();
         iter != forwardingInfo.end(); ++iter) {
        ForwardingInfo &fInfo = *iter;

        // -- Only handle responses
        if (fInfo.forwarded)
            continue;

        // -- only handle non-empty sender --> so that can send message back
        if (!fInfo.sender.empty())
            continue;

        // -- match type of ID LOOKUP
        if (fInfo.type != DM_IDLOOKUP)
            continue;

        // -- only handle reponse from a forwarded lookup
        if (!(fInfo.requestID == msg->getRequestID()))
            continue;

        // -- message should destine somebody else
        if (!RoutingID::equal(fInfo.target, msg->getTarget()))
            continue;

        fInfo.forwarded = true;
        fInfo.callback->routingIdLookupSuccess();
        Monitor::instance.addSuccessMsg(msg->getId());

        break;
    }

}

void RoutingComponent::routeIdLookupMessageBack(RoutingMessage *msg)
{
    //debugOUT << "___routeIdLookupMessageBack___" << endl;

    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin(); iter != forwardingInfo.end(); ++iter) {
        ForwardingInfo &fInfo = *iter;
        if (!fInfo.forwarded && !fInfo.sender.empty()) {
            if (fInfo.type == msg->getType()
                    && ((fInfo.type == DM_IDLOOKUP) || fInfo.requestID == msg->getRequestID())
                    && RoutingID::equal(fInfo.target, msg->getTarget())) {
                RoutingMessage *duplicate = msg->dup();
                duplicate->setSrcNodeID(peer.getNodeID().c_str());
                duplicate->setDestNodeID(fInfo.sender.c_str());
                duplicate->setTreeID(fInfo.treeID);
                duplicate->setRequestID(fInfo.requestID);
                peer.sendMessage(duplicate);
                fInfo.forwarded = true;

                //cout << "message forwarded" << endl;
                break;
            }
        }
    }

}

void RoutingComponent::handleStorage(RoutingMessage *msg) {
    //debugOUT << "___handleStorage___" << endl;

#ifdef LOGGING
    ROUTING_LOG() << peer.getNodeID() << " is now storing" << msg->getTarget()->toString() << "(" << msg->getFile() << ")" << " in tree " << msg->getTreeID() << endl;
#endif

    //check if content is already known
    Content *c;
    if (content.find(msg->getTarget()) != content.end()) {
        c = content[msg->getTarget()];
    } else { //unknown content, create new
        c = new Content(msg->getTarget(), msg->getFile(), numberOfTrees, false, minNeighborStableTime, redistributionInterval);
        content[msg->getTarget()] = c;
    }

    //right now the content object exists, but the peer is not responsible for the content in any tree.
    //but of course it must be responsible in the tree the storage request was routed in, so let's change that here.
    c->redistributionInfo[msg->getTreeID()].storageRequestReceived();

    //create and send response
    RoutingMessage *response = new RoutingMessage;
    response->setType(DM_STORAGE);
    response->setSrcNodeID(peer.getNodeID().c_str());
    response->setDestNodeID(msg->getSrcNodeID());
    response->setTreeID(msg->getTreeID());
    response->setTarget(msg->getTarget());
    response->setRequestID(msg->getRequestID());
    response->setResponse(true);
    peer.sendMessage(response);
}

void RoutingComponent::storageRequestSeen(RoutingMessage *msg) {
    //debugOUT << "___storageRequestSeen___" << endl;

    //if we happen to have the content, reschedule its redistribution in this tree. It would not make sense
    //to redistribute it soon, because there is already a redistribution running right now.
    if (content.find(msg->getTarget()) != content.end()) {
        content[msg->getTarget()]->redistributionInfo[msg->getTreeID()].scheduleNextRedistribution();
    }
}

void RoutingComponent::handleStorageConfirm(RoutingMessage *msg) {
    //debugOUT << "___handleStorageConfirm___" << endl;

    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin(); iter != forwardingInfo.end(); ++iter) {
        ForwardingInfo &fInfo = *iter;
        //check if fInfo was not handled, if it is for a request initiated at this peer (that means no sender)
        //also check if the confirm message fits the request. Comparing RequestIDs would probably be enough, but let's compare everything to reduce probability of requestID collision
        //It should not hurt so much since the other comparisons only happen when RequestIDs are equal
        if (!fInfo.forwarded && fInfo.sender.empty() && fInfo.requestID == msg->getRequestID()
                && fInfo.type == msg->getType() && RoutingID::equal(fInfo.target, msg->getTarget()) ) {
#ifdef LOGGING
            ROUTING_LOG() << peer.getNodeID() << " now handles storage confirm! " << endl;
#endif
            //find content and call storageRequestSuccess
            //this peer will then loose responsibility for the content in the corresponding tree.
            //After that, if the peer is no longer responsible for the content in any tree, the content should be deleted. However that is done in update()
            if (content.find(fInfo.target) != content.end()) {
                content[fInfo.target]->redistributionInfo[fInfo.treeID].storageRequestSuccess();
#ifdef LOGGING
                //ROUTING_LOG() << peer.getNodeID() << " no longer stores " << fInfo.target->toString() << "(" << contentIter->file << ")" << " in tree " << fInfo.treeID << endl;
                ROUTING_LOG() << peer.getNodeID() << " no longer stores " << fInfo.target->toString() << " in tree " << fInfo.treeID << endl;
#endif
            }
            fInfo.forwarded = true;
        }
    }
}

void RoutingComponent::handleLookup(RoutingMessage *msg) {
    //debugOUT << "___handleLookup___" << endl;

    //if the content is unknown to this peer, the answer will be an empty string, indicating that the content was not found
    string result = "";
    if (content.find(msg->getTarget()) != content.end()) {
        result = content[msg->getTarget()]->file;
        //requestReceived() will 'remember', when the content was requested for the last time. that info can be used as a criteria to delete or keep content, if there is too much.
        content[msg->getTarget()]->requestReceived();
    }

#ifdef LOGGING
    ROUTING_LOG() << peer.getNodeID() << " handles lookup for " << msg->getTarget()->toString() << "(" << result << ")" << " in tree " << msg->getTreeID() << endl;
#endif

    RoutingMessage *response = new RoutingMessage;
    response->setType(DM_LOOKUP);
    response->setSrcNodeID(peer.getNodeID().c_str());
    response->setDestNodeID(msg->getSrcNodeID());
    response->setTreeID(msg->getTreeID());
    response->setTarget(msg->getTarget());
    response->setRequestID(msg->getRequestID());
    response->setFile(result.c_str());
    response->setResponse(true);
    peer.sendMessage(response);
}

void RoutingComponent::handleLookupResponse(RoutingMessage *msg) {
    //debugOUT << "___handleLookupResponse___" << endl;

    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin(); iter != forwardingInfo.end(); ++iter) {
        ForwardingInfo &fInfo = *iter;
        //check if a response was not already handled for fInfo and if it is a ForwardingInfo instance not created for forwarding a response but actually handling it
        //(indicated by missing sender)
        if (!fInfo.forwarded && fInfo.sender.empty()) {
            //check if fInfo type is lookup, if the requestIDs are equal and - to reduce chance of requestID collision - if targets are equal
            //However if msg contains a result, so lookup was a success, we do NOT compare requestIDs, since that would mean risking
            //That the response we are actually looking for will not arrive or will be a failure. Why not just take this one, since it is a success and it's for the same target!
            if (fInfo.type == DM_LOOKUP && (msg->getFile()[0] || fInfo.requestID == msg->getRequestID())
                    && RoutingID::equal(fInfo.target, msg->getTarget())) {
                fInfo.forwarded = true;
                string file = msg->getFile();
                if (file.empty()) { // failure
                    if (advancedRoutingStrategy) {
                        if (fInfo.remainingAttempts == 0) {
                            lookupInOtherTrees(fInfo, false);
                            if (fInfo.remainingAttempts > 0)
                                fInfo.forwarded = false;
                        } else if (fInfo.remainingAttempts == 1) {
                            fInfo.callback->routingFailure(fInfo.target);
                            fInfo.callback->routingBadResponseFailure(fInfo.target);
                        } else {
                            fInfo.remainingAttempts--;
                        }
                    } else {//normal routing - failure means failure
                        fInfo.callback->routingFailure(fInfo.target);
                        fInfo.callback->routingBadResponseFailure(fInfo.target);
                    }
                }
                else { // success
                    fInfo.callback->routingSuccess(fInfo.target, file);
                }

            }
        }
    }
}

void RoutingComponent::lookupInOtherTrees(ForwardingInfo &fInfo, bool timeout) {
    //debugOUT << "___lookupInOtherTrees___" << endl;

    //1. check if we now have the content (unlikely but possible)
    if (content.find(fInfo.target) != content.end()) {
        fInfo.callback->routingSuccess(fInfo.target, content[fInfo.target]->file);
    }

    //2. try routing in all trees except fInfo.treeID
    //count the number of trees in fInfo.remainingAttempts
    for (int treeID = 0; treeID < numberOfTrees; treeID++) {
        if (treeID == fInfo.treeID)
            continue;

        string nextHop = getNextHop(fInfo.target, treeID);
        if (nextHop.empty())
            continue;

        RoutingMessage *msg = new RoutingMessage;
        msg->setType(DM_LOOKUP);
        msg->setSrcNodeID(peer.getNodeID().c_str());
        msg->setDestNodeID(nextHop.c_str());
        msg->setTreeID(treeID);
        msg->setTarget(fInfo.target);
        msg->setRequestID(fInfo.requestID);
        peer.sendMessage(msg);
        fInfo.remainingAttempts++;
    }

    //3. if no lookups where done (i.e. no next hops in any tree were found) we call failure or timeout on callback
    //depending on the reason why this method was called in the first place
    if(fInfo.remainingAttempts == 0) {
        if (timeout) {
            fInfo.callback->routingTimeout(fInfo.target);
        } else {
            fInfo.callback->routingFailure(fInfo.target);
            fInfo.callback->routingNoNextHopFailure(fInfo.target);
        }
    }
}

bool RoutingComponent::forwardMessage(RoutingMessage *msg) {
    //cout << "___forwardMessage___" << endl;

    // -- Update TTL
    if(msg->getTTL() == 0) {
        cout << "Zero TTL, the message should be deleted" << endl;
        return false;
    }

    //get the next hop in the tree, the message is routed in. If there is no next hop, return false - we will then handle the message
    string nextHop = getNextHop(msg->getTarget(), msg->getTreeID());
    if (nextHop.empty())
        return false;

    msg->setTTL(msg->getTTL()-1);
    //cout << " -- TTL = " << msg->getTTL() << endl;


    //store info, so that we can route back the answer later
    ForwardingInfo fInfo(msg);
    forwardingInfo.push_back(fInfo);


    //copy message and send to next hop
    RoutingMessage *duplicate = msg->dup();
    duplicate->setSrcNodeID(peer.getNodeID().c_str());
    duplicate->setDestNodeID(nextHop.c_str());
    peer.sendMessage(duplicate);
    return true;
}

void RoutingComponent::routeMessageBack(RoutingMessage *msg) {
    //debugOUT << "___routeMessageBack___" << endl;

    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin(); iter != forwardingInfo.end(); ++iter) {
        ForwardingInfo &fInfo = *iter;
        //check if fInfo was not yet used for forwarding AND if it is supposed to be used for forwarding at all (by checking for sender)
        //(i.e. it was not created for a request initiated at this peer)
        if (!fInfo.forwarded && !fInfo.sender.empty()) {
            //check type and target and of course requestID BUT only if this is not a positive response for a lookup!
            //(just as in handleLookupResponse)
            if (fInfo.type == msg->getType() && ((fInfo.type == DM_LOOKUP && msg->getFile()[0]) || fInfo.requestID == msg->getRequestID())
                    && RoutingID::equal(fInfo.target, msg->getTarget())) {
                RoutingMessage *duplicate = msg->dup();
                duplicate->setSrcNodeID(peer.getNodeID().c_str());
                duplicate->setDestNodeID(fInfo.sender.c_str());
                duplicate->setTreeID(fInfo.treeID);
                duplicate->setRequestID(fInfo.requestID);
                peer.sendMessage(duplicate);
                fInfo.forwarded = true;
    #ifdef LOGGING
                ROUTING_LOG() << peer.getNodeID() << " forwarded message back to " << fInfo.sender << endl;
    #endif
            }
        }
    }

}

void RoutingComponent::messageReceived(RoutingMessage *msg) {
    //debugOUT << "___messageReceived___" << endl;

    if (msg->getType() == DM_STORAGE) {
        if (!msg->getResponse()) { //storage request
            //check for cycles, then try to forward message to next peer. If that fails, this peer must be responsible for the content and store it.
            if (!requestAlreadyHandled(msg)) {
                //try to forward message to next peer
                if (forwardMessage(msg)) {
                    //message was forwarded to a closer neighbor.
                    storageRequestSeen(msg);
                } else {
                    //message was not forwarded - the peer is the final hop and must handle the storage request
                    handleStorage(msg);
                }
            }
        } else { //storage response
            //the first call will check if the original request came from another peer and will forward the confirmation message.
            //the second call will check if the request was initiated from the peer itself and will handle the confirmation in that case.
            //the reason for doing both instead of for example first check if the request originated here and only forward it, if that was false, is:
            //Both scenarios can be true at the same time. We can have started a storage request our self and also have forwarded a storage request for the same content from another peer.
            //(though this may be more likely in the case of lookups instead of storages, it is still a posibility)
            routeMessageBack(msg);
            handleStorageConfirm(msg);
        }
    }
    else if (msg->getType() == DM_LOOKUP) {
        if (!msg->getResponse()) { //lookup request
            //check cycles. Then check if either this peer knows the content (may be true even though it is not the closest peer, because content is not always at the best location, since the tree can always change),
            //or if the lookup request could not be forwarded. In that case the lookup failed. In both cases we call handleLookup, which will send the response.
            if (!requestAlreadyHandled(msg) && (hasContent(msg->getTarget()) || !forwardMessage(msg)))
                handleLookup(msg);
        } else { //lookup response
            //again we do try to forward the response back AND handle it ourself, if the request was initiated from this peer.
            //(read comment for storage response for details)
            routeMessageBack(msg);
            handleLookupResponse(msg);
        }
    }
    else if (msg->getType() == DM_IDLOOKUP) {
        if (!msg->getResponse()) { //lookup request
            if (!forwardMessage(msg))
                handleIdLookup(msg);
        } else { //lookup response
            routeIdLookupMessageBack(msg);
            handleIdLookupResponse(msg);
        }
    }
}

string RoutingComponent::getNextHop(RoutingIDPtr target, int treeID) {
    //debugOUT << "___getNextHop___" << endl;

    string result = "";
    int bestDist = -1;
    if (ownRoutingIDs[treeID]) {
        bestDist = target->distance(ownRoutingIDs[treeID]);
    }

    for (map<string, RoutingIDPtr>::iterator iter = routingTables[treeID].begin();
         iter != routingTables[treeID].end(); ++iter) {
        int dist = target->distance(iter->second);
        if (bestDist == -1 || dist < bestDist) {
            bestDist = dist;
            result = iter->first;
        }
    }

    return result;
}

int RoutingComponent::getBestTree(RoutingIDPtr target, string &nextHop) {
    //debugOUT << "___getBestTree___" << endl;

    //cout << "target = " << target->toString() << endl;
    //cout << "target = " << target->toString() << endl;
    //cout << "target = " << target->toString() << endl;

    int bestDist = -1;
    int bestTree = -1;
    nextHop = "";

    for (int treeID = 0; treeID < numberOfTrees; treeID++) {
        //get shortest distance for one of the trees
        int tmpDist = -1;
        string tmpNextHop = getNextHop(target, treeID);

        //cout << "tmpNextHop = " << tmpNextHop << endl;
        //cout << "tmpNextHop = " << tmpNextHop << endl;
        //cout << "tmpNextHop = " << tmpNextHop << endl;

        if (tmpNextHop.empty()) {
            //it no better neighbor was found, but we have a routingID, then we are responsible for the target.
            if (ownRoutingIDs[treeID]) {
                tmpDist = target->distance(ownRoutingIDs[treeID]);
            }
        } else {
            tmpDist = target->distance(routingTables[treeID][tmpNextHop]);
        }

        if (tmpDist != -1) {
            if (bestDist == -1 || tmpDist < bestDist) {
                bestDist = tmpDist;
                bestTree = treeID;
                nextHop = tmpNextHop;
            }
        }
    }

    return bestTree;
}

void RoutingComponent::printOwnRoutingIDs()
{
    cout << "Number of routing IDs: " << ownRoutingIDs.size() << endl;
    for (std::vector<RoutingIDPtr>::iterator it = ownRoutingIDs.begin();
         it != ownRoutingIDs.end(); ++it) {
        cout << "\t " << (*it)->toString() << endl;
    }
}

vector<RoutingIDPtr> RoutingComponent::getOwnRoutingIds()
{
    assert(ownRoutingIDs.size() > 0);
    //for (vector<RoutingIDPtr>::iterator it = ownRoutingIDs.begin(); it != ownRoutingIDs.end(); ++it)
    //    cout << (*it)->toString() << endl;
    return (ownRoutingIDs);
}

RoutingIDPtr RoutingComponent::getRouingIdInTree(int treeID)
{
    assert(treeID < (int)ownRoutingIDs.size());
    return (ownRoutingIDs[treeID]);
}

unsigned RoutingComponent::getHash(string file, unsigned number) {
    //debugOUT << "___getHash___" << endl;

    unsigned hash = 0;
    for (unsigned i = 0; i < file.size(); i++) {
        hash = number * hash + file[i];
    }
    return hash;
}

RoutingIDPtr RoutingComponent::createIDForContent(string file) {
    //debugOUT << "___createIDForContent___" << endl;

    unsigned primes[] = {157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439};
    RoutingIDPtr result = RoutingID::create();
    //cout << "original result: " << result->toString() << endl;
    for (int i = 0; i < contentIDLength; i++) {
        //get a content dependent but seemingly random number and append it to the prefix
        //unfortunately I have not found a formula to create RoutingIDs which are always free of patterns.
        //Occasionally IDs like 1,1,1,1,1... or 3,5,9,3,5,9,3,... will occur!
        unsigned prime = primes[i % (sizeof(primes) / sizeof(unsigned))];
        RoutingIDPtr suffix = RoutingID::create((getHash(file, prime) % prime) % contentIDRange);
        result = result->append(suffix);
    }
    //cout << "result after adding suffix: " << result->toString() << endl;

    return result;
}


unsigned long RoutingComponent::generateRequestID() {
    return peer.getRNG(0)->intRand();
}

bool RoutingComponent::requestAlreadyHandled(RoutingMessage *msg) {
    //debugOUT << "___requestAlreadyHandled___" << endl;

    for (list<ForwardingInfo>::iterator iter = forwardingInfo.begin(); iter != forwardingInfo.end(); ++iter) {
        //comparison of requestIDs and treIDs might be enough but we compare a few additional things, to reduce the risk of random requestID collisions
        if (iter->requestID == msg->getRequestID() && iter->treeID == msg->getTreeID() && iter->type == msg->getType() && RoutingID::equal(iter->target, msg->getTarget()))
            return true;
    }
    return false;
}





