#include "PrefixRoutingID.h"
#include <iomanip>

//#define TKENV_
#ifdef TKENV_
   #define debugOUT EV
#else
   #define debugOUT (!false) ? std::cout : std::cout \
   << "@" << std::setprecision(8) << simTime().dbl() \
   << "::PrefixRoutingID: "
#endif

int PrefixRoutingID::distance(const RoutingIDPtr other) const {
    debugOUT << "___distance___" << endl;

    if (!other)//should never happen!
        return 0;

    const PrefixRoutingID &_other = *static_cast<const PrefixRoutingID *>(other.get());
    std::vector<int>::const_iterator iter1 = prefix.begin();
    std::vector<int>::const_iterator iter2 = _other.prefix.begin();

    unsigned commonPartLength = 0;

    while (prefix.size() > commonPartLength && _other.prefix.size() > commonPartLength && *iter1 == *iter2) {
        iter1++;
        iter2++;
        commonPartLength++;
    }

    return prefix.size() + _other.prefix.size() - 2*commonPartLength;
}
