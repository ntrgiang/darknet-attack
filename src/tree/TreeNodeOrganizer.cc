//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "TreeNodeOrganizer.h"
#include "logging.h"
#include "Peer.h"
#include "StringTreeNodeID.h"
#include "Monitor.h"
#include "AppCommon.h"
#include <iomanip>


//#define TKENV_
#ifdef TKENV_
   #define debugOUT EV
#else
   #define debugOUT (!false) ? std::cout : std::cout \
   << "@" << std::setprecision(8) << simTime().dbl() \
   << "::TreeNodeOrganizer: "
#endif


TreeNodeOrganizer::TreeNodeOrganizer(Peer &peer) : peer(peer) {
    periodicalEvent = new cMessage();
    updateInterval = peer.par("updateInterval");

    int numberOfRealities = peer.par("numberOfRealities");

    routingComponent = new RoutingComponent(peer, numberOfRealities);

    for (int i = 0; i < numberOfRealities; i++) {
        //create random treeNodeID (from a string, which we create first)
        char buffer[100];
        unsigned long randomNumber = (i == 0 ? 0 : peer.getRNG(0)->intRand());
        sprintf(buffer, "%012lu%d%s", randomNumber, i, peer.getNodeID().c_str()); //set id to random value (and add treeID and host name for uniqueness - should not be necessary)
        TreeNodeIDPtr treeNodeID(new StringTreeNodeID(string(buffer)));
        treeNodes.push_back(new TreeNode(peer, *this, treeNodeID, i));
    }

    //printIdAllTrees();
}

TreeNodeOrganizer::~TreeNodeOrganizer() {
    peer.cancelAndDelete(periodicalEvent);

    for (unsigned i = 0; i < treeNodes.size(); i++) {
        delete treeNodes[i];
    }

    delete routingComponent;
}

void TreeNodeOrganizer::messageReceived(DarknetMessage* msg) {
    //cout << "received a msg of type " << typeToString(msg) << endl;

    if (msg->getType() == DM_HELLO) {
        helloReceived(static_cast<HelloMessage *>(msg));
    } else if (msg->getType() == DM_GOODBY) {
        goodbyReceived(msg);
    } else if (msg->getType() == DM_STORAGE
               || msg->getType() == DM_LOOKUP
               || msg->getType() == DM_IDLOOKUP) {
        RoutingMessage *routingMsg = static_cast<RoutingMessage *>(msg);
        routingComponent->messageReceived(routingMsg);
    }
}

void TreeNodeOrganizer::helloReceived(HelloMessage* msg) {
    //debugOUT << "___helloReceived___" << endl;

    string peerID = msg->getSrcNodeID();
    HelloInfoContainer helloInfos = msg->getHelloInfos();
    for (unsigned i = 0; i < helloInfos.size(); i++) {
        TreeNode *responsibleNode = treeNodes[helloInfos[i].treeID];
        responsibleNode->helloReceived(helloInfos[i], peerID);
    }
}

void TreeNodeOrganizer::goodbyReceived(DarknetMessage *msg) {
#ifdef LOGGING
    ALL_LOGS() << peer.getNodeID() << " received GOODBY from " << msg->getSrcNodeID() << "\n";
#endif
    for (unsigned i = 0; i < treeNodes.size(); i++)
        treeNodes[i]->goodbyReceived(msg->getSrcNodeID());
}

void TreeNodeOrganizer::handleActivation() {
    //debugOUT << "___handleActivation___" << endl;

    routingComponent->activated();

    for (unsigned i = 0; i < treeNodes.size(); i++)
        treeNodes[i]->activated();


    collectAndSendHellos();
    peer.scheduleEvent(periodicalEvent, simTime() + updateInterval, this);
}

void TreeNodeOrganizer::handleDeactivation() {
    for (unsigned i = 0; i < treeNodes.size(); i++)
        treeNodes[i]->deactivated();

    //send goodby messages to all neighbors
    for (unsigned i = 0; i < peer.getNeighbors().size(); i++) {
        string &peerID = peer.getNeighbors()[i];
        DarknetMessage *msg = new DarknetMessage;
        msg->setSrcNodeID(peer.getNodeID().c_str());
        msg->setDestNodeID(peerID.c_str());
        msg->setType(DM_GOODBY);
        peer.sendMessage(msg);
    }

    routingComponent->deactivated();

    peer.cancelEvent(periodicalEvent);
}

void TreeNodeOrganizer::handleEvent(cMessage *event) {
    //debugOUT << "___handleEvent___" << endl;

    if (event == periodicalEvent) {
        routingComponent->update();
        collectAndSendHellos();
        peer.scheduleEvent(periodicalEvent, simTime() + updateInterval, this);
    }

}

// -- WHY this function has not been called
void TreeNodeOrganizer::collectAndSendHellos() {
    //debugOUT << "___collectAndSendHellos___" << endl;

    vector<string> &neighbors = peer.getNeighbors();
    map<string, HelloInfoContainer> allHelloInfos;

    //debugOUT << "Print neighbor list of " << peer.getNodeID() << ": " << neighbors.size() << endl;
    //for (vector<string>::iterator it = neighbors.begin(); it != neighbors.end(); ++it)
    //    debugOUT << "\t " << *it << endl;

    //create empty hello containers for every neighbor
    for (unsigned i = 0; i < neighbors.size(); i++) {
        allHelloInfos[neighbors[i]] = HelloInfoContainer();
    }

    //allow all TreeNodes to fill the containers if they need to
    for (unsigned treeID = 0; treeID < treeNodes.size(); treeID++) {
        treeNodes[treeID]->update(allHelloInfos);
    }

    //send hello to all neighbors for which at least one hello was accumulated
    //debugOUT << "send Hello message" << endl;
    for (map<string, HelloInfoContainer>::iterator iter = allHelloInfos.begin(); iter != allHelloInfos.end(); ++iter) {
        if (iter->second.size() > 0) {
            HelloMessage *msg = new HelloMessage();
            msg->setHelloInfos(iter->second);
            msg->setSrcNodeID(peer.getNodeID().c_str());
            msg->setDestNodeID(iter->first.c_str());
            msg->setType(DM_HELLO);
            peer.sendMessage(msg);
        }
    }
}

void TreeNodeOrganizer::printIdAllTrees() {
    cout << "___printIdAllTrees___" << endl;

    cout << "Node id in all trees:" << endl;
    for (vector<TreeNode *>::iterator it = treeNodes.begin(); it != treeNodes.end(); ++it) {
        cout << (*it)->getTreeNodeId()->toString() << endl;
    }
}






