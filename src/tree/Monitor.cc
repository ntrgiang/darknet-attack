//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#include "Monitor.h"
#include <algorithm>
//#include <queue>

Monitor Monitor::instance;


RoutingIDPtr Monitor::getRandomNodeId(unsigned int tree)
{
    vector<RoutingIDPtr> treeIds;
    for (map<string, map<unsigned int, RoutingIDPtr> >::iterator it = m_routingIdList.begin();
         it != m_routingIdList.end(); ++it) {
        if (RoutingID::equal(RoutingIDPtr(), it->second[tree]))
            continue;

        treeIds.push_back(it->second[tree]);
    }

    if (treeIds.size() == 0)
        return RoutingIDPtr();

    return (treeIds[intrand(treeIds.size())]);
}

vector<RoutingIDPtr> Monitor::getAllRoutingIdPtr(std::string nodeID)
{
    map<string, map<unsigned int, RoutingIDPtr> >::iterator it = m_routingIdList.find(nodeID);
    if (it == m_routingIdList.end())
        return (vector<RoutingIDPtr>());

    vector<RoutingIDPtr> ret;
    for (map<unsigned int, RoutingIDPtr>::iterator iter = it->second.begin();
         iter != it->second.end(); ++iter) {
        ret.push_back(iter->second);
    }
    return (ret);
}

std::string Monitor::findNodeId(RoutingIDPtr rId)
{
    string ret;
    ret.clear();
    for (map<string, map<unsigned int, RoutingIDPtr> >::iterator it = m_routingIdList.begin();
         it != m_routingIdList.end(); ++it) {

        vector<RoutingIDPtr> allId = getAllRoutingIdPtr(it->first);
        if (allId.size() == 0)
            continue;
        vector<RoutingIDPtr>::iterator iter = find(allId.begin(), allId.end(), rId);
        if (iter != allId.end()) {
            ret = it->first;
            break;
        }
    }

    assert(!ret.empty());
    return (ret);
}

void Monitor::storeOneRoutingId(const std::string hostname, unsigned int tree, const RoutingIDPtr id)
{
    map<string, map<unsigned int, RoutingIDPtr> >::iterator iter = m_routingIdList.find(hostname);
    if (iter == m_routingIdList.end()) {
        // no entry for the hostname
        map<unsigned int, RoutingIDPtr> a_map;
        a_map.insert(std::pair<unsigned int, RoutingIDPtr>(tree, id));
        m_routingIdList.insert(std::pair<string, map<unsigned int, RoutingIDPtr> >
                                (hostname, a_map));

//        m_routingIdTable.insert(std::pair<string, map<unsigned int, RoutingIDPtr> >
//                                (hostname, map<unsigned int, RoutingIDPtr>(std::pair<unsigned int, RoutingIDPtr>(tree, id)
//                                                                           )));
    } else { // exists an entry for the hostname
        // -- check if the ID for the respective tree exists
//        cout << "Current IDs of host " << iter->first << endl;
//        map<unsigned int, RoutingIDPtr>::iterator it = iter->second.begin();
//        for (; it != iter->second.end(); ++it) {
//            cout << "\t " << it->first
//                 << " -- " << it->second->toString() << endl;
//        }

//        it = iter->second.find(tree);
//        if (it != iter->second.end()) {
//            cout << "change of ID in tree " << tree
//                 << " -- current ID: " << it->second->toString() << endl;
//        }
        iter->second[tree] = id;
    }
}

void Monitor::printRoutingIdList()
{
    cout << "There are " << m_routingIdList.size() << " hosts registered" << endl;
    for (map<string, map<unsigned int, RoutingIDPtr> >::iterator it = m_routingIdList.begin();
         it != m_routingIdList.end(); ++it) {
        cout << "\t Host " << it->first << endl;
        for (map<unsigned int, RoutingIDPtr>::iterator iter = it->second.begin();
             iter != it->second.end(); ++iter) {
            cout << "\t\t ID in tree " << iter->first << " -- " << iter->second->toString() << endl;
        }
    }
}

void Monitor::reportHopCount(int hopCount)
{
    m_hopCounts.push_back(hopCount);
}

void Monitor::reportJoinTime(double joinTime)
{
    m_joinTimes.push_back(joinTime);
    m_maxJoinTime = (m_maxJoinTime < joinTime) ? joinTime : m_maxJoinTime;
}

double Monitor::getMaxJoinTime()
{
    return m_maxJoinTime;
}

void Monitor::reportRootChange(int treeID, double time)
{
    m_rootChanges.insert(std::pair<int, double>(treeID, time));
    //cout << "treeID " << treeID << " -- time " << time << endl;
}

void Monitor::addFriendList(std::string host, vector<std::string> friends)
{
    //cout << "___addFriendList___" << endl;

    //cout << "host " << host << " has friends: " << friends.size() << endl;
    //for (vector<string>::iterator it = friends.begin(); it != friends.end(); ++it) {
    //    cout << "\t " << *it << endl;
    //}

    assert(m_friendList.find(host) == m_friendList.end());
    m_friendList.insert(std::pair<string, vector<string> >(host, friends));
    m_offlineNodes.push_back(host);
}

void Monitor::printFriendList()
{
    cout << "___printFriendList___" << endl;
    for (map<string, vector<string> >::iterator it = m_friendList.begin();
         it != m_friendList.end(); ++it) {
        cout << "host " << it->first << " has friends: " << it->second.size() << endl;
        for (vector<string>::iterator iter = it->second.begin();
             iter != it->second.end(); ++iter) {
            cout << "\t " << *iter << endl;
        }
    }
}

void Monitor::reportOnlineNode(std::string nodeID)
{
    //cout << "___reportOnlineNode___" << endl;

    // -- Delete nodeID from the offline list (if any)
    vector<string>::iterator it = find(m_offlineNodes.begin(), m_offlineNodes.end(), nodeID);
    if (it != m_offlineNodes.end())
        m_offlineNodes.erase(it);

    // -- Make sure that nodeID is NOT in the online list
    it = find(m_onlineNodes.begin(), m_onlineNodes.end(), nodeID);
    assert(it == m_onlineNodes.end());

    m_onlineNodes.push_back(nodeID);
    assert(m_friendList.size() == m_onlineNodes.size() + m_offlineNodes.size());

    //printOnlineNodes();
    //printOfflineNodes

    findConnectdComponents();
}


void Monitor::reportOfflineNode(std::string nodeID)
{
    //cout << "___reportOfflineNode___" << endl;

    // -- Delete nodeID from the online list (if any)
    vector<string>::iterator it = find(m_onlineNodes.begin(), m_onlineNodes.end(), nodeID);
    if (it != m_onlineNodes.end())
        m_onlineNodes.erase(it);

    // -- Make sure that nodeID is NOT in the online list
    it = find(m_offlineNodes.begin(), m_offlineNodes.end(), nodeID);
    assert(it == m_offlineNodes.end());

    m_offlineNodes.push_back(nodeID);
    assert(m_friendList.size() == m_onlineNodes.size() + m_offlineNodes.size());

    //printOfflineNodes();
    //printOnlineNodes();

    findConnectdComponents();
}

void Monitor::printOnlineNodes()
{
    cout << "Online nodes: " << m_onlineNodes.size() << endl;
    for (vector<string>::iterator it = m_onlineNodes.begin(); it != m_onlineNodes.end(); ++it) {
        cout << "\t " << *it << endl;
    }
}

void Monitor::printOfflineNodes()
{
    cout << "Offline nodes: " << m_offlineNodes.size() << endl;
    for (vector<string>::iterator it = m_offlineNodes.begin(); it != m_offlineNodes.end(); ++it) {
        cout << "\t " << *it << endl;
    }
}

void Monitor::printConnectedComponents()
{
    cout << "___printConnectedComponents___" << endl;
    cout << "Number of components: " << m_connectedComponents.size() << endl;
    for (map<int, vector<string> >::iterator it = m_connectedComponents.begin();
         it != m_connectedComponents.end(); ++it) {
        cout << "\t all nodes of component " << it->first << " -- " << it->second.size() << endl;
        for (vector<string>::iterator iter = it->second.begin();
             iter != it->second.end(); ++iter) {
            cout << "\t\t " << *iter << endl;
        }
    }
}

void Monitor::addLookupMsg(long msgTreeId)
{
    m_lookupMessages.push_back(msgTreeId);
}

void Monitor::addIncorrectRoutingMsg(long msgTreeId)
{
    m_incorrectRoutingMessages.push_back(msgTreeId);
}

void Monitor::addSuccessMsg(long msgTreeId)
{
    m_successMessages.push_back(msgTreeId);
}

vector<long> Monitor::getLookupMessages()
{
    return (m_lookupMessages);
}

vector<long> Monitor::getIncorrectRoutingMessages()
{
    return (m_incorrectRoutingMessages);
}

vector<long> Monitor::getSuccessMessages()
{
    return (m_successMessages);
}

void Monitor::printLookupMessages()
{
    cout << "___printLookupMessages___" << endl;
    cout << "Number of elements: " << m_lookupMessages.size() << endl;
    for (vector<long>::iterator it = m_lookupMessages.begin();
         it != m_lookupMessages.end(); ++it) {
        cout << *it << " ";
    }
    cout << endl;
}

void Monitor::printIncorrectRoutingMessages()
{
    cout << "___printIncorrectRoutingMessages___" << endl;
    cout << "Number of elements: " << m_incorrectRoutingMessages.size() << endl;
    for (vector<long>::iterator it = m_incorrectRoutingMessages.begin();
         it != m_incorrectRoutingMessages.end(); ++it) {
        cout << *it << " ";
    }
    cout << endl;
}

void Monitor::printSuccessMessages()
{
    cout << "___printSuccessMessages___" << endl;
    cout << "Number of elements: " << m_successMessages.size() << endl;
    for (vector<long>::iterator it = m_successMessages.begin();
         it != m_successMessages.end(); ++it) {
        cout << *it << " ";
    }
    cout << endl;
}


void Monitor::findConnectdComponents()
{
    //cout << "___findConnectdComponents___" << endl;
    //cout << "==================================================" << endl;
    //cout << "==================================================" << endl;
    //cout << "==================================================" << endl;

    assert(m_onlineNodes.size() > 0);
    m_connectedComponents.clear();

    vector<string> unbrowsedNodes = m_onlineNodes;

    //cout << "unbrowsedNodes" << endl;
    //printList(unbrowsedNodes);

    int component = 0;

    while (unbrowsedNodes.size() > 0) {
        //cout << "\t **********************************" << endl;
        //cout << "\t **********************************" << endl;
        std::queue<string> myQueue;
        myQueue.push(unbrowsedNodes[0]);
        vector<string> aComponent;
        aComponent.push_back(unbrowsedNodes[0]);

        // -- Find all nodes for a component
        while (myQueue.size() > 0) {
            //cout << "\t -----------------" << endl;
            //cout << "\t -----------------" << endl;

            string aNode = myQueue.front();
            myQueue.pop();

            deleteNodeFromList(unbrowsedNodes, aNode);

            //cout << "unbrowsedNodes" << endl;
            //printList(unbrowsedNodes);
            vector<string> friends = m_friendList[aNode];

            //cout << "friends" << endl;
            //printList(friends);
            for (vector<string>::iterator it = friends.begin(); it != friends.end(); ++it) {
                if (!nodeIsOnline(*it))
                    continue;

                //cout << "node is online: " << *it << endl;
                if (!inList(unbrowsedNodes, *it))
                    continue;

                //cout << "node is not browsed" << endl;
                myQueue.push(*it);
                deleteNodeFromList(unbrowsedNodes, *it);
                aComponent.push_back(*it);

                //cout << "aComponent" << endl;
                //printList(aComponent);
            }
        }

        //cout << "\t -- a component is found" << endl;
        //printList(aComponent);

        // -- a component should be found here
        m_connectedComponents.insert(std::pair<int, vector<string> >(component++, aComponent));
    }

    //printConnectedComponents();
    //assert(false);

}

int Monitor::getComponentNumber(std::string nodeID)
{
    int ret = -1;
    map<int, vector<string> >::iterator it = m_connectedComponents.begin();
    for (; it != m_connectedComponents.end(); ++it) {
        vector<string>::iterator iter = find(it->second.begin(), it->second.end(), nodeID);
        if (iter != it->second.end())
            break;
    }
    ret = it->first;
    assert(ret >= 0);
    return ret;
}

bool Monitor::inSameComponent(std::string localNodeID, std::string remoteNodeID)
{
    int component1 = getComponentNumber(localNodeID);
    int component2 = getComponentNumber(remoteNodeID);

    if (component1 == component2)
        return (true);
    return (false);
}

bool Monitor::nodeIsOnline(std::string nodeID)
{
    return (find(m_onlineNodes.begin(), m_onlineNodes.end(), nodeID) != m_onlineNodes.end());
}

bool Monitor::inList(vector<std::string> l, std::string nodeID)
{
    return (find(l.begin(), l.end(), nodeID) != l.end());
}

void Monitor::deleteNodeFromList(vector<std::string> &list, std::string nodeID)
{
    vector<string>::iterator it = find(list.begin(), list.end(), nodeID);
    if (it != list.end())
        list.erase(it);
}

void Monitor::printList(vector<std::string> l)
{
    cout << "elements of list: " << l.size() << endl;
    for (vector<string>::iterator it = l.begin(); it != l.end(); ++it) {
        cout << "\t " << *it << endl;
    }
}

void Monitor::printQueue(std::queue<std::string> q)
{
    cout << "elements of queue: " << q.size() << endl;
    while (q.size() > 0) {
        cout << "\t " << q.front() << endl;
        q.pop();
    }
}
