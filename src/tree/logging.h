/*
 * logging.h
 *
 *  Created on: 05.06.2013
 *      Author: Matthias
 */

#ifndef LOGGING_H_
#define LOGGING_H_

//uncomment for enabling logging.
//#define LOGGING


#include <fstream>
#include <map>
#include <string>
#include <omnetpp.h>

//this stream will silently drop all input. It is used, when logging is disabled.
extern std::ofstream nullStream;

extern std::map<int, std::ofstream *> logs;

//get the stream for the log-file of a tree
std::ofstream &getLog(int treeID, bool printTime = true);

//in order to write to all tree log-files simultaneously while still having the feeling of working with only one stream object,
//we need this class which implements << and simply writes the given argument to all tree log-files
struct MultiStream {

    template<typename T>
    MultiStream &operator<<(T something) {
        for (std::map<int, std::ofstream *>::iterator iter = logs.begin(); iter != logs.end(); ++iter) {
            (*iter->second) << something;
        }
        return *this;
    }

};

extern MultiStream _ALL_LOGS;

//the return value of this function can be treated as one stream object, but it will write the content to all tree log-files.
//It will also add the current simulation time at the beginning.
//for some reason it does not accept endl (and probably some other stream specific things, too) but it works for string, int, double, etc.
inline MultiStream &ALL_LOGS() {
    char _time_buffer[20];
    sprintf(_time_buffer, "%010.4f|", simTime().dbl());
    _ALL_LOGS << _time_buffer;
    return _ALL_LOGS;
}

extern std::ofstream routingLog;

//returns a reference to the stream for the routing log-file. Also it adds the current simulation time at the beginning.
inline std::ofstream &ROUTING_LOG(bool printTime = true) {
    if (printTime) {
        char _time_buffer[20];
        sprintf(_time_buffer, "%010.4f|", simTime().dbl());
        routingLog << _time_buffer;
    }
    return routingLog;
}

#endif /* LOGGING_H_ */
