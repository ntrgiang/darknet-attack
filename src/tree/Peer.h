//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef PEER_H_
#define PEER_H_

#include "DarknetBaseNode.h"
#include "TreeNodeOrganizer.h"
#include "Monitor.h"

class Peer : public DarknetBaseNode {
    bool active;
    double par_meanOffPeriod;

    cMessage *activationEvent;
    cMessage *testLookupEvent;
    cMessage *testStorageEvent;

    cMessage *timer_idLookup;
    double m_expectedLeaveTime;

    std::map<cMessage *, EventHandler *> eventHandlers;

    TreeNodeOrganizer *treeNodeOrganizer;

    std::vector<std::string> neighbors;

    //these variables make sure, some global initialization / finalization operations are only performed once and not for every peer.
    //(I guess this is not the normal OMNET++ way for doing this.)
    static bool globalInitDone;
    static bool globalFinishDone;

    //all (de)activation times of all peers read from the configuration file for the current network. (If the parameter activationTimesFile exists for the network)
    static std::map<std::string, list<double> > activationTimes;

    virtual void setActive(bool newActive);

    //parameters for routing test
    bool testStorage;
    bool testLookup;
    bool par_idLookup;

    unsigned int par_numRealities; // Total number of realities
    unsigned int par_tau;          // number of lookup trees

    //parameters stating when the routing tests will begin
    simtime_t lookupTestBegin;
    simtime_t storageTestBegin;
    simtime_t minWaitForLookup;
    //initially this is set to the maximum number of allowed storage operations. It is decreased with ever such operation until it reaches zero.
    int remainingStorageTests;
    //used to evaluate the result of lookup tests (only is created when testLookup is true)
    //for logging, every instance knows the nodeID of the peer, therefore one instance is needed for each peer (otherwise one global instance would be enough)
    RoutingResultCallback *callbackInstance;

public:
    Peer();
    virtual ~Peer();

    virtual int numInitStages() const { return 6;}
    virtual void initialize(int stage);
    //global initialization (= for all peers). Will only be called on initialization of the first peer.
    virtual void globalInit();
    virtual void finish();
    virtual void readActivationTimes(const char *fileName);

    virtual void connectPeer(const std::string& toNodeID, const IPvXAddress& destAddr, int destPort);
    virtual void handleSelfMessage(cMessage* msg);
    virtual void handleIncomingMessage(DarknetMessage *msg);
    virtual void handleForeignMessage(DarknetMessage *msg);
    virtual DarknetPeer *findNextHop(DarknetMessage *msg);


    virtual void sendMessage(DarknetMessage* msg) {DarknetBaseNode::sendMessage(msg);}

    //virtual std::string &getNodeID() {return nodeID;}
    virtual std::vector<std::string> &getNeighbors() {return neighbors;}
    void printNeighbors();

    virtual bool isActive() {return active;}

    /*
     * schedules the next activation (or deactivation, if the peer is active).
     * if an entry in activationTimes exists for the peer's nodeID, the next value will be extracted from the list and the (de)activation will be scheduled acordingly.
     * if the extracted value is 0, the scheduled time will be randomly selected according to this peer's parameters. In that case the 0 value is not removed from the
     * list, which means the peer will keep activate and deactivate randomly. random scheduling will also happen, if no entry for the host exists in activationTimes.
     * if an entry exists in activationTimes but no values are left, nothing is done. The host will never change is activation state again.
     */
    virtual void scheduleActivation();

    //If an event is already scheduled you must not schedule it again, until the event is handled!
    virtual void scheduleEvent(cMessage *event, simtime_t t, EventHandler *handler);
    virtual cMessage *cancelEvent(cMessage *event);
    virtual void cancelAndDelete(cMessage *event);

    //test storage and lookup (though only lookup will result in a measurable response)
    //these methods get called according the the parameters set for this peer.
    virtual void performLookupTest();
    virtual void performStorageTest();
    virtual void performIdLookup();

protected:
    void printActivationTimes();

    static int m_numReportedFirstJoinTime;
};

#endif /* PEER_H_ */
