/*
 * StringTreeNodeID.h
 *
 *  Created on: 28.05.2013
 *      Author: Matthias
 */

#ifndef STRINGTREENODEID_H_
#define STRINGTREENODEID_H_

#include <string>
#include "IDs.h"

class StringTreeNodeID : public TreeNodeID {
    std::string id;
public:
    StringTreeNodeID(std::string id) : id(id) {}
    virtual ~StringTreeNodeID(){}

    virtual int compareTo(const TreeNodeIDPtr other) const {
        return id.compare(static_cast<const StringTreeNodeID *>(other.get())->id);
    }

    virtual std::string toString() const {return id;}
};


#endif /* STRINGTREENODEID_H_ */
