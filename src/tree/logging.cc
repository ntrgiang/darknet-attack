/*
 * logging.cc
 *
 *  Created on: 05.06.2013
 *      Author: Matthias
 */




#include "logging.h"
#include <cstring>

using namespace std;

ofstream nullStream(0);

map<int, ofstream *> logs;

ofstream &getLog(int treeID, bool printTime) {
    if (logs.find(treeID) == logs.end()) {
        char fileName[100];
        sprintf(fileName, "log%02d.txt", treeID);
        logs[treeID] = new ofstream(fileName);
    }
    if (printTime) {
        char _time_buffer[20];
        sprintf(_time_buffer, "%010.4f|", simTime().dbl());
        (*logs[treeID]) << _time_buffer;
    }
    return *logs[treeID];
}


MultiStream _ALL_LOGS;



ofstream routingLog("routingLOG.txt");
