/*
 * IDs.h
 *
 *  Created on: 05.06.2013
 *      Author: Matthias
 */

#ifndef IDS_H_
#define IDS_H_

#include "boost/shared_ptr.hpp"
#include <string>

class TreeNodeID;

typedef boost::shared_ptr<TreeNodeID> TreeNodeIDPtr;

class TreeNodeID {
public:
    virtual ~TreeNodeID(){}

    //other must be a valid non-null pointer
    //note: since all TreeNodeIDs should be unique, you can check for equality by comparing 2 (smart) pointers with ==
    virtual int compareTo(const TreeNodeIDPtr other) const = 0;

    virtual std::string toString() const = 0;

private:
};

class RoutingID;

typedef boost::shared_ptr<RoutingID> RoutingIDPtr;

class RoutingID {
public:
    //this returns an instance of a concrete RoutingID implementation (therefore it cannot be implemented in this header)
    static RoutingIDPtr create();
    static RoutingIDPtr create(int offsetIndex);

    virtual ~RoutingID(){}

    //must be zero if and only if both routingIDs are equal
    virtual int distance(const RoutingIDPtr other) const = 0;

    //this might only make sense for prefixes.
    //if you don't need it, you don't have to implement it
    //this default implementation does not make any sense and is only there so that you can omit
    //this method entirely if you don't need it in your RoutingID implementation
    virtual RoutingIDPtr append(const RoutingIDPtr other) {return RoutingIDPtr();}

    //get hash for use in data structures like unordered_map or unordered_set
    virtual std::size_t hash() const = 0;

    //two NULL-pointers are considered as equal!
    //since the same RoutingID can exist several times, you should NEVER compare RoutingID pointers with ==
    static bool equal(const RoutingIDPtr id1, const RoutingIDPtr id2) {
        if (!id1 && !id2)
            return true;
        else if (!id1 || !id2)
            return false;
        else
            return (id1->distance(id2)) == 0;
    }

    virtual std::string toString() const = 0;

};

//functor class for use in hash based data structures
struct RoutingIDPtrEqual {
    bool operator()(const RoutingIDPtr& id1, const RoutingIDPtr& id2) const
     {
        return RoutingID::equal(id1, id2);
     }
};

//functor class for use in hash based data structures
struct RoutingIDPtrHash {
    std::size_t operator()(const RoutingIDPtr& id) const
     {
         return id->hash();
     }
};


//this wrapper is needed since I get compile errors when trying to put a RoutingIDPtr into an omnet msg-file
//the problem is that omnet automatically generates a << operator for the pointer and since that is already defined, the compiler complains
//I tried to make this wrapper look and feel as much as the pointer it contains, as possible.
class RoutingIDPtrWrapper {
    RoutingIDPtr ptr;
public:
    RoutingIDPtrWrapper(){}
    RoutingIDPtrWrapper(RoutingIDPtr ptr) : ptr(ptr) {}

    operator RoutingIDPtr() const {
        return ptr;
    }

    RoutingID *operator->() {
        return ptr.operator ->();
    }

    RoutingID &operator*() {
        return ptr.operator *();
    }

};


#endif /* IDS_H_ */
