/*
 * Content.h
 *
 *  Created on: 16.07.2013
 *      Author: Matthias
 */

#ifndef CONTENT_H_
#define CONTENT_H_

#include <string>
#include <vector>
#include "IDs.h"

#include <omnetpp.h>

using namespace std;

//for every content instance we will have one Redistribution instance per treeID
//they will tell us when to redistribute the content in the individual trees
struct Redistribution {

    //true if we are responsible for the content in this tree (usually a node will only be responsible in one tree)
    bool responsible;

    //true, if the a storage request for this content was sent and we are waiting for the result
    bool moving;

    //currently known neighbor closest to the content's ID. if no better neighbor is known, this will be the empty string.
    string betterNeighbor;

    //last time when betterNeighbor changed. with this we can determine, if betterNeighbor is somewhat stable. If the tree is currently heavily changing, betterNeighbor may change very often and it would be
    //a bad idea, to send any content to it right then.
    simtime_t lastBetterNeighborChange;

    //planned time for the next redistribution of the content in this tree. this will be reset, if a distribution (in this tree) from another node is detected.
    simtime_t nextRedistribution;

    //how long a better neighbor must be stable before a storage message is sent to it. (this can be set via storage_minNeighborStableTime parameter)
    simtime_t minNeighborStableTime;

    //used to determine nextRedistribution
    simtime_t redistributionInterval;

    Redistribution(simtime_t minNeighborStableTime, simtime_t redistributionInterval) : minNeighborStableTime(minNeighborStableTime), redistributionInterval(redistributionInterval) {
        scheduleNextRedistribution();
    }

    virtual ~Redistribution(){}

    //set best known neighbor in this tree. if newBetterNeighbor is empty, that means there is no better neighbor than the peer itself.
    virtual void setBetterNeighbor(string newBetterNeighbor) {
        if (betterNeighbor != newBetterNeighbor) {
            betterNeighbor = newBetterNeighbor;
            lastBetterNeighborChange = simTime();
        }
    }

    //called when a storage message for this content was sent to betterNeighbor
    //until the result of that storage request is known, we won't start any other storage requests for this content in this tree.
    virtual void storageRequestStarted() {
        moving = true;
    }

    //called if the storage request failed. We stay responsible and are now ready again for sending a new storage request to betterNeighbor.
    virtual void storageRequestTimeout() {
        moving = false;
    }

    //called when the storage request was confirmed. We drop responsibility for the content in this tree.
    //also we won't redistribute the content in this tree for a while (while we were responsible we would try to redistribute as soon as we saw a better neighbor.
    //But now we only redistribute after a certain period of time. Just in case the nodes actually responsible in this tree go offline.)
    virtual void storageRequestSuccess() {
        moving = false;
        responsible = false;
        scheduleNextRedistribution();
    }

    //if we receive a storage request for this content we will take responsibility for the content in this tree.
    //also, since a redistribution was just done in this tree (even if not by us) we reset the interval for the next redistribution
    //though since we are now responsible for the content in this tree, we will redistribute it, as soon as a stable better neighbor is found.
    virtual void storageRequestReceived() {
        responsible = true;
        scheduleNextRedistribution();
    }

    //plan the time when the content will be distributed in this tree the next time. That time will be a little random, so not all peers redistribute a content at the same time.
    virtual void scheduleNextRedistribution() {
        nextRedistribution = simTime() + redistributionInterval;
    }

    //true if the content should be redistributed in this tree.
    //true if the planed nextRedistribution time has passed, or if we are responsible for the content in this tree, but a better neighbor was found.
    virtual bool redistributionNecessary() {
        if (moving)
            return false; //don't redistribute it while we already have a pending redistribution running

        if (betterNeighbor.empty() || simTime() - lastBetterNeighborChange < minNeighborStableTime)
            return false; //no (stable) better neighbor --> no redistribution (no matter whether or not we are responsible in this tree)!

        if (responsible)
            return true;//if there is a better neighbor and we are responsible in this tree, we redistribute the content, so that is will always be at the best location

        if (simTime() > nextRedistribution)
            return true; //even if we are not responsible in this tree, if the content wasn't redistributed in a long while, we will do it, to prevent it from being lost!

        return false;
    }

    //after a peer is activated it will claim responsibility for all content it knows in all trees.
    //Also some other info that may be left from the previous active phase must be cleared
    virtual void initAfterActivation() {
        responsible = true;
        moving = false;
        betterNeighbor = "";
        lastBetterNeighborChange = simTime();
    }

};

struct Content {
    bool newContent;
    RoutingIDPtr ID;
    string file;
    simtime_t lastRequest;

    vector<Redistribution> redistributionInfo;

    //if the content is coming from this peer, you want isResponsible to be true, because at first, the peer is responsible for the content in all trees,
    //before redistributing it to better neighbors.
    //if this content is created because of a received storage request, isResponsible has to be false and storageRequestReceived() must be called after the constructor!
    Content(RoutingIDPtr ID, string file, int numberOfTrees, bool isResponsible, simtime_t minNeighborStableTime, simtime_t redistributionInterval) : newContent(true), ID(ID), file(file) {
        lastRequest = simTime();

        Redistribution redist(minNeighborStableTime, redistributionInterval);
        redist.lastBetterNeighborChange = simTime();
        redist.moving = false;
        redist.responsible = isResponsible;

        redistributionInfo.resize(numberOfTrees, redist);
    }

    virtual ~Content(){}

    //true, if this node is not responsible for the content any more (in all trees)
    //note that this does not mean, that you have to actually delete it. If there is enough space you can also keep it, to potentially speed up lookups.
    virtual bool canBeDeleted() const {
        bool notResponsible = true;
        for (unsigned i = 0; i < redistributionInfo.size(); i++)
            if (redistributionInfo[i].responsible) {
                notResponsible = false;
                break;
            }
        return notResponsible;
    }

    //called when a lookup for the content was received. the last time the content was requested is used for ordering Content objects, so that
    //recently requested content comes first and less popular content comes after that and may be deleted
    virtual void requestReceived() {
        lastRequest = simTime();
    }

    //this content is considered smaller, when it is more important (recently requested)
    //and bigger, when it is unimportant (not requested, or we are not really responsible for it in any tree)
    virtual bool operator<(const Content &other) const {
        if (canBeDeleted())
            return false;
        if (other.canBeDeleted())
            return true;
        return lastRequest < other.lastRequest;
    }

    //use for sorting if you store a list of content pointers, instead of content objects. (not used currently)
    static bool comparePointers(Content *p1, Content *p2) {
        return *p1 < *p2;
    }
};


#endif /* CONTENT_H_ */
