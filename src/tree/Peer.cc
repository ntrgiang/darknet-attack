//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#include "Peer.h"
#include <iomanip>

Define_Module(Peer);

using namespace std;
using namespace boost;

//#define TKENV_
#ifdef TKENV_
   #define debugOUT EV
#else
   #define debugOUT (!m_debug) ? std::cout : std::cout \
   << "@" << setprecision(8) << simTime().dbl() \
    << " Peer " << getNodeID() << ": "
#endif

//callback for evaluating the results of lookups.
struct TestLookupCallback : public RoutingResultCallback {
    string nodeID;

    TestLookupCallback(string nodeID) : nodeID(nodeID) {}

    void routingIdLookup() {
        //cout << "___routingIdLookup___" << endl;
        Monitor::instance.idLookup++;
    }
    void routingIdLookupSuccess() {
        //cout << "___routingSuccess___" << endl;
        Monitor::instance.idLookupSuccess++;
    }

    void routingSuccess(RoutingIDPtr target, string file) {
        Monitor::instance.success++;
#ifdef LOGGING
        ROUTING_LOG() << nodeID << " : lookup successful for " << target->toString() << "(" << file << ")" << endl;
#endif
    }

    void routingFailure(RoutingIDPtr target) {
        Monitor::instance.failure++;
#ifdef LOGGING
        ROUTING_LOG() << nodeID << " : lookup failed for " << target->toString() << endl;
#endif
    }

    void routingTimeout(RoutingIDPtr target) {
        Monitor::instance.timeout++;
#ifdef LOGGING
        ROUTING_LOG() << nodeID << " : lookup timeout for " << target->toString() << endl;
#endif
    }

    void routingNoCloserNeighborFailure(RoutingIDPtr target) {
        Monitor::instance.noCloserNeighborFailure++;
#ifdef LOGGING
        ROUTING_LOG() << nodeID << " : lookup failed (NoCloserNeighbor) for " << target->toString() << endl;
#endif
    }

    void routingNoNextHopFailure(RoutingIDPtr target) {
        Monitor::instance.noNextHopFailure++;
#ifdef LOGGING
        ROUTING_LOG() << nodeID << " : lookup failed (NoNextHop) for " << target->toString() << endl;
#endif
    }

    void routingBadResponseFailure(RoutingIDPtr target) {
        Monitor::instance.badResponseFailure++;
#ifdef LOGGING
        ROUTING_LOG() << nodeID << " : lookup failed (BadResponse) for " << target->toString() << endl;
#endif
    }

};


bool Peer::globalInitDone;
bool Peer::globalFinishDone;
map<string, list<double> > Peer::activationTimes;
//int Peer::m_numInitiatedNodes = 0;

Peer::Peer() {}

Peer::~Peer() {}

void Peer::initialize(int stage) {
    DarknetBaseNode::initialize(stage);

    if (stage == 3) {
        // -- Report friend list
        Monitor::instance.addFriendList(nodeID, getFriendList());
        //vector<string> test = getFriendList();
    }

    if (stage == 5) {
        for (map<string, DarknetPeer>::iterator iter = peers.begin(); iter != peers.end(); ++iter) {
            neighbors.push_back(iter->first);
        }
        //printNeighbors();

        callbackInstance = 0;
        globalInitDone = false;
        globalFinishDone = false;
        active = false;
        m_expectedLeaveTime = -1.0;

        treeNodeOrganizer = new TreeNodeOrganizer(*this);
        activationEvent = new cMessage("activationEvent");
        testLookupEvent = new cMessage("testLookupEvent");
        testStorageEvent = new cMessage("testStorageEvent");
        timer_idLookup = new cMessage("TIMER_ID_LOOKUP");

        if (!globalInitDone) {
            globalInit();
            globalInitDone = true;
        }

        par_numRealities    = par("numberOfRealities");
        par_tau             = par("tau");
        testLookup      = par("testLookup");
        testStorage     = par("testStorage");
        par_idLookup    = par("idLookup");

        lookupTestBegin = par("lookupTestBegin").doubleValue();
        storageTestBegin = par("storageTestBegin").doubleValue();
        minWaitForLookup = par("minWaitForLookup").doubleValue();
        remainingStorageTests = par("maxNumberOfStorageTests");

        par_meanOffPeriod = par("meanOffPeriod").doubleValue();
        debugOUT << "par_meanOffPeriod = " << par_meanOffPeriod << endl;

        scheduleActivation();
        if (testLookup || par_idLookup)
            callbackInstance = new TestLookupCallback(nodeID);
    }


}

void Peer::globalInit() {
    debugOUT << "___globalInit___" << endl;

    //this should get us the network, so we can read parameters specified for the entire network and not for single peers.
    Monitor::instance.initialize(this);

    string activationTimesFile = par("activationTimesFile");
    debugOUT << "activationTimesFile = " << activationTimesFile.c_str() << endl;
    if (!activationTimesFile.empty())
        readActivationTimes(activationTimesFile.c_str());
}

void Peer::finish() {
    if (!globalFinishDone) {
        Monitor::instance.finish();
        globalFinishDone = true;
    }

    // -- Events
    //
    cancelAndDelete(activationEvent);
    cancelAndDelete(testLookupEvent);
    cancelAndDelete(testStorageEvent);
    cancelAndDelete(timer_idLookup);
    delete treeNodeOrganizer;
    delete callbackInstance;
}

void Peer::readActivationTimes(const char *fileName) {
    debugOUT << "___readActivationTimes___" << endl;

    ifstream input(fileName);
    if (!input.is_open())
        debugOUT << "file " << fileName << " cannot be opened" << endl;
    //TODO: check why the file cannot be read!!!

    //TODO: the following block of code is executed several times <-- fix it!!!
    string line;
    while (getline(input, line)) {
        //check if the line is empty, i.e. contains nothing but whitespace characters
        if (line.find_first_not_of(" \t\n\r") == line.npos)
            continue;
        //find first space to separate hostname and list of activation times
        int spacePos = line.find(' ');
        string host = line.substr(0, spacePos);
        string timePart = line.substr(spacePos);

        stringstream convert(timePart);
        list<double> times;
        double nextTime;
        while (convert >> nextTime) {
            times.push_back(nextTime);
        }
        activationTimes[host] = times;
    }

    //printActivationTimes();
}

void Peer::connectPeer(const std::string& toNodeID, const IPvXAddress& destAddr, int destPort) {}


void Peer::handleSelfMessage(cMessage* msg) {
    if (msg == activationEvent) {
        setActive(!isActive());
    } else if (msg == testLookupEvent) {
        performLookupTest();
        scheduleAt(simTime() + par("lookupTestInterval").doubleValue(), testLookupEvent);
    } else if (msg == testStorageEvent) {
        performStorageTest();
        scheduleAt(simTime() + par("storageTestInterval").doubleValue(), testStorageEvent);
    } else if (msg == timer_idLookup) {
        if (isActive()) {
            performIdLookup();
            scheduleAt(simTime() + par("lookupTestInterval").doubleValue(), timer_idLookup);
        }
    } else {
        debugOUT << "periodical event" << endl;

        EventHandler *handler = eventHandlers[msg];
        eventHandlers.erase(msg);
        handler->handleEvent(msg);
    }
}

void Peer::handleIncomingMessage(DarknetMessage *msg) {
    debugOUT << "___handleIncomingMessage___" << endl;

    //do stuff with the message that is related to peer state and network details (e.g. drop the message if the peer is not active, or simulate a bad connection by randomly dropping messages)
    if (!isActive()) {
        if (msg->getType() == DM_IDLOOKUP)
            Monitor::instance.dropByChurn++;
        delete msg;
        return;
    }

    //forward message to TreeNodeOrganizer
    treeNodeOrganizer->messageReceived(msg);

    delete msg;
}

void Peer::handleForeignMessage(DarknetMessage *msg) {
    //not allowed!
}

DarknetPeer *Peer::findNextHop(DarknetMessage *msg) {
    return &peers[msg->getDestNodeID()];
}

void Peer::printNeighbors()
{
    cout << "There are " << neighbors.size() << " neighbors" << endl;
    for (std::vector<std::string>::iterator it = neighbors.begin(); it != neighbors.end(); ++it) {
        cout << "\t - " << *it << endl;
    }

}


void Peer::setActive(bool newActive) {
    debugOUT << "___setActive___" << endl;

    if (active == newActive) return;

    active = newActive;
    if (newActive) {
#ifdef LOGGING
        ALL_LOGS() << nodeID << " activated at " << simTime().str() << "\n";
#endif

        // -- Connected components
        //Monitor::instance.reportOnlineNode(nodeID);

        debugOUT << "Should activate NOW" << endl;
        treeNodeOrganizer->handleActivation();

        //on activation start with routing tests
        if (testLookup) {
            if (simTime() < lookupTestBegin)
                scheduleAt(lookupTestBegin, testLookupEvent);
            else
                scheduleAt(simTime() + par("lookupTestInterval"), testLookupEvent);
        }

        if (testStorage) {
            if (simTime() < storageTestBegin)
                scheduleAt(storageTestBegin, testStorageEvent);
            else
                scheduleAt(simTime() + par("storageTestInterval"), testStorageEvent);
        }

        // -- ID lookup
        if (par_idLookup) {
            if (!timer_idLookup->isScheduled()) {
                //if (simTime() < Monitor::instance.getMaxJoinTime())
                //    scheduleAt(Monitor::instance.getMaxJoinTime() + 1000 + dblrand(), timer_idLookup);
                if (simTime() < lookupTestBegin)
                    scheduleAt(lookupTestBegin, timer_idLookup);
                //if (simTime().dbl() < par("lookupTestBegin").doubleValue())
                //    scheduleAt(par("lookupTestInterval").doubleValue() + rand(), timer_idLookup);
                else
                    scheduleAt(simTime() + par("lookupTestInterval"), timer_idLookup);
            }
        }

    }
    else {
#ifdef LOGGING
        ALL_LOGS() << nodeID << " shutdown at " << simTime().str() << "\n";
#endif
        // -- Connected components
        //Monitor::instance.reportOfflineNode(nodeID);

        //cancel all events
        map<cMessage *, EventHandler *> eventHandlersCopy = eventHandlers;
        for (map<cMessage *, EventHandler *>::iterator iter = eventHandlersCopy.begin(); iter != eventHandlersCopy.end(); ++iter)
            cancelEvent(iter->first);

        treeNodeOrganizer->handleDeactivation();

        //no routing tests, while peer is offline
        cancelEvent(testLookupEvent);
        cancelEvent(testStorageEvent);
    }

    scheduleActivation();

}

void Peer::scheduleActivation() {
    debugOUT << "___scheduleActivation___" << endl;

    simtime_t nextTime;
    bool getRandomly = false;
    if (activationTimes.find(nodeID) == activationTimes.end()) {
        debugOUT << "nodeID not found in activationTimes" << endl;
        //nodeID not found in activationTimes
        getRandomly = true;
    } else {
        list<double> &times = activationTimes[nodeID];
        if (times.empty()) {
            //no more values --> no further scheduling
            return;
        }
        else if (times.front() == 0) {
            getRandomly = true;
        } else {
            nextTime = times.front();
            times.pop_front();
        }
    }

    if (getRandomly) {
        if (isActive()) {
            //
            // Peer is active now --> schedule leave
            //
            nextTime = simTime() + par("nextDeparture").doubleValue();
            m_expectedLeaveTime = nextTime.dbl();
            debugOUT << "nextTime = " << nextTime << endl;
            if (!m_static)
                scheduleAt(nextTime, activationEvent);
            else
                debugOUT << "Statis scenario --> peers won't leave" << endl;
        } else {
            //
            // Peer is inactive now --> schedule JOIN
            //

            // -- Old code
            // nextTime = simTime() + par("nextArrival").doubleValue();

            // -- New code
            const double MAX_SIM_TIME = 9e6;
            double nextPeriod = exponential(par_meanOffPeriod);
            nextPeriod = (nextPeriod > MAX_SIM_TIME) ? MAX_SIM_TIME : nextPeriod;
            nextTime = simTime() + nextPeriod;
            //cout << "nextTime = " << nextTime << endl;
            scheduleAt(nextTime, activationEvent);
            Monitor::instance.reportJoinTime(nextTime.dbl());
            //if (m_numReportedFirstJoinTime )
        }
    }
}

void Peer::scheduleEvent(cMessage *event, simtime_t t, EventHandler *handler) {
    if (!event->isScheduled()) {
        scheduleAt(t, event);
        eventHandlers[event] = handler;
    }
}

cMessage *Peer::cancelEvent(cMessage *event) {
    eventHandlers.erase(event);
    return cSimpleModule::cancelEvent(event);
}

void Peer::cancelAndDelete(cMessage *event) {
    eventHandlers.erase(event);
    cSimpleModule::cancelAndDelete(event);
}

void Peer::performLookupTest() {
    debugOUT << "___performLookupTest___" << endl;

    //treeNodeOrganizer->routingComponent->printOwnRoutingIDs();

    vector<StoredFile> &files = Monitor::instance.storedFiles;
    cRNG *rng = getRNG(0);

    if (files.size() > 0) {
        //we will start with a random stored file. If it was stored not too recently, we will try to look it up. Otherwise we will check the next one and so on, until we find a file
        //that is old enough to look it up. (We don't perform lookup on very recently stored files, because it is clear that they probably won't have spread through the network yet.)
        int randomIndex = rng->intRand() % (files.size());
        for (unsigned i = 0; i < files.size(); i++) {
            StoredFile &fileToLookup = files[(randomIndex + i) % files.size()];
            if (simTime() - fileToLookup.creationTime > minWaitForLookup) {
                //of course we don't lookup the file directly - what would be the point in looking up a file we know already
                //lookup is done via the file's ID
                RoutingIDPtr fileID = treeNodeOrganizer->routingComponent->createIDForContent(fileToLookup.file);
                treeNodeOrganizer->routingComponent->lookup(fileID, callbackInstance);
                break;
            }
        }
    }
}

void Peer::performStorageTest() {
    debugOUT << "___performStorageTest___" << endl;

    vector<StoredFile> &files = Monitor::instance.storedFiles;
    cRNG *rng = getRNG(0);

    if (remainingStorageTests > 0) {
        remainingStorageTests--;

        //create a random file
        string file;
        int size = 20 + rng->intRand() % 20;

        for (int i = 0; i < size; i++) {
            char c = 'A' + rng->intRand() % (2*26);
            if (c > 'Z')
                c += 'a' - 'Z' - 1;
            file.append(1, c);
        }

        //store the file in the network
        RoutingIDPtr fileID = treeNodeOrganizer->routingComponent->store(file);
        //and remember it for future lookup tests
        files.push_back(StoredFile(file, fileID));

    }
}

void Peer::performIdLookup()
{
    vector<unsigned int> treeList;
    for (unsigned int i = 0; i < par_numRealities; ++i) {
        treeList.push_back(i);
    }

    // -- Shuffle the treeList
    unsigned int listSize = treeList.size();
    for (unsigned int i = listSize-1; i > 0; --i)
    {
       unsigned j = intrand(i+1);
       unsigned int temp = treeList[i];
       treeList[i] = treeList[j];
       treeList[j] = temp;
    }

    int count = 0;                 // count of attempts
    const int MAX_COUNT = 10000;    // max number of attempts
    while (count < (int)par_tau) {
        if (count >= MAX_COUNT) // give up after so many attemps
            break;

        int treeID = treeList[count++ % treeList.size()];
        RoutingIDPtr aRoutingId = Monitor::instance.getRandomNodeId(treeID);
        assert(aRoutingId);
        if (RoutingID::equal(aRoutingId, treeNodeOrganizer->routingComponent->getOwnRoutingIds()[treeID]))
            continue;
        debugOUT << "a random routing Id = " << aRoutingId->toString() << endl;

        // -- Make sure that remoteNodeID is not the nodeID itself
        //getOwnRoutingIds
        if (RoutingID::equal(treeNodeOrganizer->routingComponent->getRouingIdInTree(treeID), aRoutingId)) {
            cout << "aRoutingId " << aRoutingId->toString() << " is node's ID itself" << endl;
            continue;
        }

        bool lookupSent = false;
        treeNodeOrganizer->routingComponent->idLookup(treeID, aRoutingId, callbackInstance, lookupSent);

        // -- Finding out why a lookup cannot be sent out from start
        //
        if (!lookupSent) {
            string remoteNodeID = Monitor::instance.findNodeId(aRoutingId);
            //cout << "remoteNodeID = " << remoteNodeID << endl;
            //cout << "my nodeID = " << nodeID << endl;

            if (Monitor::instance.inSameComponent(nodeID, remoteNodeID)) {
                //cout << "2 nodes are in the same component" << endl;
                Monitor::instance.incrementNumSameComponent();
            }
            else {
                //cout << "2 nodes are in two different components" << endl;
                Monitor::instance.incrementNumDiffComponent();
            }
        }
    }

    /*
    // -- get another list which contain the first /tau/ element of the shuffled list
    vector<unsigned int> lookupTreeIds;
    for (unsigned int i = 0; i < par_tau; ++i) {
        lookupTreeIds.push_back(treeList[i]);
    }

    //cout << "Tree IDs of the random lookup list: " << lookupTreeIds.size() << endl;
    //for (vector<unsigned int>::iterator it = lookupTreeIds.begin(); it != lookupTreeIds.end(); ++it) {
    //    cout << "\t " << *it << endl;
    //}

    // -- For each tree, send one LOOKUP message
    for (vector<unsigned int>::iterator it = lookupTreeIds.begin();
         it != lookupTreeIds.end(); ++it) {
        //cout << "!!!!! LOOKUP FOR TREE " << *it << endl;
        unsigned treeID = *it;
        RoutingIDPtr aRoutingId = Monitor::instance.getRandomNodeId(treeID);
        if (RoutingID::equal(aRoutingId, treeNodeOrganizer->routingComponent->getOwnRoutingIds()[treeID]))
            continue;
        assert(!(RoutingID::equal(aRoutingId, treeNodeOrganizer->routingComponent->getOwnRoutingIds()[treeID])));
        if (RoutingID::equal(RoutingIDPtr(), aRoutingId)) {
            debugOUT << "empty routing id" << endl;
            return;
        }
        debugOUT << "a random routing Id = " << aRoutingId->toString() << endl;
        treeNodeOrganizer->routingComponent->idLookup(treeID, aRoutingId, callbackInstance);
    }
    */
}

void Peer::printActivationTimes()
{
    cout << "___printActivationTimes___" << endl;

    if (activationTimes.size() == 0)
        cout << "there is no activation time" << endl;

    for (std::map<std::string, list<double> >::iterator it = activationTimes.begin();
         it != activationTimes.end(); ++it)
    {
        cout << "host " << it->first << " -- times " << endl;
        for (list<double>::iterator iter = it->second.begin();
             iter != it->second.end(); ++iter)
            cout << "\t " << *iter << endl;
    }
}

