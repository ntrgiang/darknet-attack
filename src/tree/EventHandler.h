/*
 * ActivationHandler.h
 *
 *  Created on: 26.05.2013
 *      Author: Matthias
 */

#ifndef EVENTHANDLER_H_
#define EVENTHANDLER_H_

#include <omnetpp.h>

class EventHandler {
public:
    virtual ~EventHandler() {}

    virtual void handleEvent(cMessage *event) = 0;
};



#endif /* EVENTHANDLER_H_ */
