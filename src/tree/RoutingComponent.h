//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef ROUTINGCOMPONENT_H_
#define ROUTINGCOMPONENT_H_
#include <string>
#include <vector>
#include <list>
#include <boost/unordered_map.hpp>
#include "IDs.h"
#include "messages/RoutingMessage_m.h"
#include <omnetpp.h>
#include "Content.h"

class Peer;

struct RoutingResultCallback {
    virtual ~RoutingResultCallback() {}
    virtual void routingIdLookup() = 0;
    virtual void routingIdLookupSuccess() = 0;
    virtual void routingSuccess(RoutingIDPtr target, string file) = 0;
    virtual void routingFailure(RoutingIDPtr target) = 0;
    virtual void routingTimeout(RoutingIDPtr target) = 0;
    virtual void routingNoCloserNeighborFailure(RoutingIDPtr target) = 0;
    virtual void routingNoNextHopFailure(RoutingIDPtr target) = 0;
    virtual void routingBadResponseFailure(RoutingIDPtr target) = 0;
};

/*
 * A ForwardingInfo instance can serve several purposes.
 * They are used to remember, which routing requests were already seen, so routing in circles is prevented.
 * They store information about the sender (if the peer is not the initiator of a request). That way a response can be forwarded to the correct peer.
 * If the peer is initiator of the request, the ForwardingInfo will hold a callback which is used to signal the result of the routing.
 */
struct ForwardingInfo {
    //the target of the routing request. It is the ID of the content that is searched or stored
    RoutingIDPtr target;
    //nodeID of the sender of this requests. If the peer is the initiator itself, this is the empty string.
    std::string sender;
    //id of tree in which routing is done.
    //a received routing response (like the result of a lookup) will be forwarded to all known neighbors which requested that content, not only
    //the neighbor for which's request the response was created. (that means a peer receives a response and may then send responses to more than one neighbor)
    //in that case the treeID for those other neighbors must be stored, so that the response can be sent in the same tree as the request was.
    int treeID;
    //type of request (same values as the type field in the actual messages, like DM_LOOKUP).
    //important for deciding what to do on timeout.
    int type;
    //the time at which this object was created. The object must only stay alive long enough to let the response be forwarded and to detect cyclic routing.
    //after a certain time span, it is deleted.
    simtime_t creationTime;
    //unique id for a request. Used to detect cyclic routing.
    unsigned long requestID;
    //if this is true, a response has already been forwarded to sender (or if the peer is the initiator of the routing request itself, the result was already signaled via callback)
    //in that case this instance only exists to prevent cyclic routing, or duplicated routing requests, etc. It's forwarding job is done!
    bool forwarded;
    //if the peer is the initiator of a routing request the result (success, failure, timeout) will be signaled to the rest of the program via this callback.
    RoutingResultCallback *callback;
    //if the request was sent to many neighbors, this number is used to keep track of how many of them have yet to answer.
    //if a response is received, indicating success, it can be handled immediately, but if it indicates failure, other neighbors might
    //still answer with a success-message, so we don't immediately act - unless it was the last neighbor, which is detected with this variable
    //a value > 0 means that this many neighbors have yet to answer
    //if the value is 0, that means only one request was sent to begin with. This differentiation between 1 and 0 may be used to distinguish an initial request directed at the best neigbhor
    //from a secondary/alternative attempt of sending requests to several neighbors (e.g. all neighbors except the one already tried) Even when only one neighbor has yet to response, this
    //variable will be 1 instead of 0
    int remainingAttempts;

    /*
     * creates a ForwadingInfo instance from a RoutingMessage. Mostly copies some data and sets creationTime to the current time.
     * obviously this can not be used to create the ForwardInfo object for the peer which initiated the routing request.
     */
    ForwardingInfo(RoutingMessage *msg) {
        creationTime = simTime();
        target = msg->getTarget();
        sender = msg->getSrcNodeID();
        treeID = msg->getTreeID();
        type = msg->getType();
        requestID = msg->getRequestID();
        forwarded = false;
        callback = 0;
        remainingAttempts = 0;
    }
};

/*
 * This component is responsible for the actual content routing. It forwards messages, stores content, keeps a routing table, etc.
 * It does not actively start a routing request on it's own accord! It only allows others to initiate such requests through the methods store() and lookup().
 * The TreeNodes must update the routing table data of this class via addRoutingTableEntry() and setOwnRoutingID().
 * There are methods for things like (de)activation and regular updates. They are called from the TreeNodeOrganizer holding the RoutingComponent.
 * This class itself does not schedule any events.
 */
class RoutingComponent {
    //max lifetime of ForwardingInfo objects. set via Peer's parameter with the same name in the ini-file
    double routingTimeOut;

    //set via parameter. That parameter should be a random distribution, so that every peer gets a slightly different value.
    //That way, not all peers knowing a content will redistribute it at the same time.
    //If one does it before the others, and the others see those messages, they will not do redistribute the content and reschedule the next time they do.
    //(Originally this was set to a different random value for EACH content. But several content storage messages be sent at the same simulation time (in update()) but in an implementation-dependent
    //actual order (depending on the order of the objects in the content-container).
    //That would result in contents getting different intervals, changing the simulation results(!), depending on some implementation details, that should have nothing to do with it.
    double redistributionInterval;

    //set via parameter. If true, after the first lookup request failed or had a timeout, lookups in all remaining trees will be started.
    bool advancedRoutingStrategy;

    //The RoutingIDs created for content will consist of contentIDLength numbers, each in the range [0, contentIDRange - 1]
    //the values are read from Peers parameters.
    int contentIDLength;
    int contentIDRange;

    //reference to peer to send messages, get parameters, etc.
    Peer &peer;
    //number of realities
    int numberOfTrees;

    // these exist for every tree - therefore we use vectors with one entry per tree

    //for every tree we have our own routingID as well as some for our neighbors (routingTables)
    //each element in these vectors corresponds to one tree
    std::vector<RoutingIDPtr> ownRoutingIDs;
    std::vector<std::map<std::string, RoutingIDPtr> > routingTables;
    //one flag for each tree. Set to true, if any known routingID in that tree changed between updates. Reset in update()
    std::vector<bool> routingInfoChanged;

    //all currently stored ForwardingInfo instances. See ForwardingInfo comments for it's purpose.
    std::list<ForwardingInfo> forwardingInfo;

    //all the content known at the peer. Note that the Content class is rather complex. Check it's comments for details.
    boost::unordered_map<RoutingIDPtr, Content *, RoutingIDPtrHash, RoutingIDPtrEqual> content;

    //the time for which a better neighbor (i.e. a neighbor to which a file should be redistributed, because it is close to the file's ID) must stay the same before the storage request
    //is actually sent. set via one of Peer's parameters.
    simtime_t minNeighborStableTime;

public:

    //stores content in the network. Initially responsibility for the content is taken in all trees.
    //but if for any tree a better neighbor is found, a storage request will be sent there.
    //the content may be removed from the peer, if it was stored at other peers for all trees.
    //this method returns the generated RoutingID for the stored file. That is the same as createIDForContent(file);
    virtual RoutingIDPtr store(std::string file);

    //searches the network for content with the given contentID. Depending on the result of the search one of callback's methods will eventually be called.
    //This can happen immediately (e.g. if the peer itself knows the content) or it can take a 'random' amount of time!
    virtual void lookup(RoutingIDPtr contentID, RoutingResultCallback *callback);

    RoutingComponent(Peer &peer, int numberOfTrees);
    virtual ~RoutingComponent();

    //delete routing infos and all ForwardingInfo objects, but keep content!
    virtual void clear();

    //must be called (by the TreeNodeOrganizer) when the peer is (de)activated
    virtual void activated();
    virtual void deactivated();

    //must be called regularly. Since this class does not schedule events by itself, this is called by the TreeNodeOrganizer
    //as part of it's regularly update.
    virtual void update();

    //methods for TreeNodes to set routing information.
    virtual void addRoutingTableEntry(std::string &neighborID, int treeID, RoutingIDPtr routingID);
    virtual void setOwnRoutingID(int treeID, RoutingIDPtr routingID);

    //helper to generate hashes for strings.
    virtual unsigned getHash(std::string file, unsigned number);
    //Deterministically creates a RoutingID for content.
    virtual RoutingIDPtr createIDForContent(std::string file);

    //create a random ID for a new request. Those IDs are used to detect circles or duplicates.
    virtual unsigned long generateRequestID();

    //this handles all incoming routing message. it will be called from TreeNodeOrganizer::messageReceived() if the message is a RoutingMessage.
    virtual void messageReceived(RoutingMessage *msg);

    //detects loops and duplicate requests. returns true, if the peer has a ForwardingInfo object for the same request that was just received.
    //same request means same target, requestID and type (though requestID would be enough the other values can help reduce the chance of requestID collisions)
    virtual bool requestAlreadyHandled(RoutingMessage *msg);

    //forwards an incoming request to the neighbor closest to the target (in the tree the request is routed in)
    //in that case true is returned. However, if the message is not forwarded, because there is no closer neighbor, false is returned.
    //if the message is forwarded, a ForwardingInfo object is created and stored.
    virtual bool forwardMessage(RoutingMessage *msg);

    //gets called when a storage request can not be forwarded any further. In this method the peer takes responsibility for the content to be stored in the tree the request was routed in.
    //this may or may not mean, that a new Content object is created, depending on whether or not the peer is already responsible for that content in an other tree.
    virtual void handleStorage(RoutingMessage *msg);

    //get called when a storage request is received, but forwarded (i.e. handleStorage() is not called)
    //if the peer happens to have that content it will reschedule redistribution in the corresponding tree.
    //Because, while we want peers to redistribute content in trees where they are not responsible for it from time to time,
    //we don't want all of them to do that - if one does it, the others, which see the storage request, should not do it.
    virtual void storageRequestSeen(RoutingMessage *msg);

    //gets called when either the peer has the content demanded in the lookup request, or the request can't be routed any further. (Of course both can happen at the same time)
    //this methods will create a response message and send it back. If the content was found at this peer, it will be included in that message, otherwise the empty string is sent back, signaling
    //a lookup failure.
    virtual void handleLookup(RoutingMessage *msg);

    //this method is called whenever a storage confirm message is received. It will check if the confirmed storage request was initiated by this node, by checking the ForwardingInfo objects.
    //if that is the case, this peer will drop responsibility for the content in the corresponding tree
    //(and then may or may not delete the content completely, depending on if it still holds responsibility in any other trees.)
    virtual void handleStorageConfirm(RoutingMessage *msg);

    //called whenever a lookup response message is received. If a corresponding lookup was initiated by this peer, it will be handled.
    //this method may also be called, if no such lookup was initiated at this peer. In that case nothing is done.
    virtual void handleLookupResponse(RoutingMessage *msg);

    //after routing fails (or timeouts) and the paramter "advancedRoutingStrategy" is set, this method gets called.
    //It performs lookups in all trees except the one the initial lookup was performed in.
    //second parameter is the reason, why lookup failed (true = timeout, false = failure)
    virtual void lookupInOtherTrees(ForwardingInfo &fInfo, bool timeout);

    //true if this peer knows the content with the given id. This is independent of the treeID. Even though usually a peer is responsible for content only in one or a few trees,
    //it can respond to lookup requests for that content in any tree.
    virtual bool hasContent(RoutingIDPtr contentID);

    //sends a received confirmation message back to all neighbors which send a request for the corresponding target.
    //These neighbors are found by iterating through the ForwardingInfo objects. All objects with the same request type and the same target are selected - even if the requestID is different.
    //This way, if for example two requests with different IDs but for the same target go over one peer and it receives a confirmation message for one of the requests, it will forward the confirmation to both requesting peers.
    //That may save some time for the peer which's request arrived later.
    virtual void routeMessageBack(RoutingMessage *msg);

    //helper that returns the nodeID of the neighbor which is closest to target (according to the current routing information)
    //if there is no closer neighbor, the empty string is returned.
    virtual std::string getNextHop(RoutingIDPtr target, int treeID);
    //helper that gets the closest neighbor to target in any tree. The treeID is returned, the nodeID of the neighbor is returned via nextHop reference.
    virtual int getBestTree(RoutingIDPtr target, std::string &nextHop);

    // -- New
    virtual void printOwnRoutingIDs();
    vector<RoutingIDPtr> getOwnRoutingIds();
    RoutingIDPtr getRouingIdInTree(int treeID);

    // - Searches the network for a host with given the treeID and destID.
    virtual void idLookup(unsigned int treeID, RoutingIDPtr destRoutingID, RoutingResultCallback *callback, bool &lookupSent);

    virtual void handleIdLookup(RoutingMessage *msg);
    virtual void handleIdLookupResponse(RoutingMessage *msg);
    virtual void routeIdLookupMessageBack(RoutingMessage *msg);
};

#endif /* ROUTINGCOMPONENT_H_ */
