//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
// 

#ifndef TREENODE_H_
#define TREENODE_H_

#include "HelloInfo.h"
#include <string>
#include <vector>
#include <list>
#include "OffsetGenerator.h"
#include <assert.h>
#include "messages/RoutingMessage_m.h"
#include "RoutingComponent.h"

#include "logging.h"

using namespace std;


class Peer;
class TreeNodeOrganizer;


class NeighborInfo {
public:
    HelloInfo hello;

    string neighborID;


    int nextHelloCounter;
    Age age;

    //set to true, after the own (self generated) updateID was received from that neighbor
    bool circleDetected;
};






class TreeNode {

    //parameter
    double minStableRoutingIDPeriod;
    double maxRootDistance;
    double helloInterval;
    double updateInterval;

    Peer &peer;
    TreeNodeOrganizer &treeNodeOrganizer;
    TreeNodeIDPtr treeNodeID;
    int treeID;


    vector<NeighborInfo> neighbors;
    OffsetGenerator *offsetGenerator;


    //state
    TreeNodeIDPtr parent;
    TreeNodeIDPtr pendingParent;

    TreeNodeIDPtr currentRoot;
    int currentRootDist;
    RoutingIDPtr currentParentRoutingID;
    RoutingIDPtr currentOffset;
    unsigned long currentUpdateID;
    unsigned long lastParentUpdateID;

    //the last time, the routingID of this node changed (NOT the currentlyUsedRoutingID, but what is returned by getFullRoutingID())
    simtime_t lastRoutingIDChange;

    bool parentChanged;
    bool helloToAll;

public:
    //this is public so the treeNodeOrganizer can use it to decide in which tree it wants to root
    RoutingIDPtr currentlyUsedRoutingID;


    TreeNode(Peer &peer, TreeNodeOrganizer &treeNodeOrganizer, TreeNodeIDPtr treeNodeID, int treeID);
    virtual ~TreeNode();

    virtual void activated();

    virtual void deactivated();

    //reset all pointers, and other clearing stuff
    virtual void clearState();

    /*
     * if neighbor is completely new, add it
     * and schedule a soon hello
     *
     * check if hello is valid. if not, block neighbor
     *
     * check if blocked neighbor may be unblocked because of a good hello
     *
     * checkParent()
     *
     * handle possible parent request
     * and schedule a soon hello for the requesting neighbor
     */
    virtual void helloReceived(HelloInfo hello, string peerID);
    virtual void goodbyReceived(string peerID);

    virtual void neighborDropped(NeighborInfo &neighborInfo);
    virtual void dropParent();
    virtual void dropPendingParent();
    virtual void dropChild(TreeNodeIDPtr childID);

    //set parent to pending parent
    virtual void changeParent();

    virtual void checkForCircle(HelloInfo &hello, NeighborInfo &neighbor);
    virtual void checkForBestParent();
    virtual bool checkStatus(NeighborInfo &parentInfo);
    virtual void updateStatus(NeighborInfo &parentInfo);

    //checks, if the treeNodeID of the sender of the hello is the same as in the last hello (which is stored in neighbor)
    //if it is not, that means the neighbor changed its treeNodeID! This could be done e.g. to let a node with many neighbors have a lower treeNodeID and
    //therefore becoming root more likely.
    //If such a change is detected, this method will behave, as if the old neighbor was dropped (like when a goodbye message is received) and then a new neighbor was added.
    //However, currently no treeNodeID change scheme is implemented!!! Therefore this method is not tested!!!
    virtual void checkForIDChange(HelloInfo &hello, NeighborInfo &neighbor);

    virtual bool canBeParent(NeighborInfo &neighbor);

    virtual void update(map<string, HelloInfoContainer> &helloContainers);

    virtual HelloInfo getHelloInfoForNeighbor(const string &peerID);

    virtual NeighborInfo &getNeighborFromPeerID(const string &peerID);
    virtual NeighborInfo &getNeighborFromTreeNodeID(TreeNodeIDPtr treeNodeID);

    //creates a new routingID by appending currentParentRoutingID and currentOffset.
    //if one of those is NULL, NULL is returned
    //if not NULL is returned, remember to delete the routingID eventually
    virtual RoutingIDPtr getFullRoutingID();


    virtual NeighborInfo createNewNeighborInfo(const string &neighborID, const HelloInfo &hello);

    virtual void generateUpdateID();


    virtual void printStatus();
    virtual TreeNodeIDPtr getTreeNodeId() { return treeNodeID; }

};

#endif /* TREENODE_H_ */
