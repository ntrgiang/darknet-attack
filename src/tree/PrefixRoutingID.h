/*
 * PrefixRoutingID.h
 *
 *  Created on: 06.06.2013
 *      Author: Matthias
 */

#ifndef PREFIXROUTINGID_H_
#define PREFIXROUTINGID_H_

#include <vector>
#include "IDs.h"
#include <cstdio>
#include <omnetpp.h>

class PrefixRoutingID : public RoutingID {
    std::vector<int> prefix;
public:
    virtual ~PrefixRoutingID() {}

    PrefixRoutingID(){}

    PrefixRoutingID(int i) {
        prefix.push_back(i);
    }


    virtual int distance(const RoutingIDPtr other) const;

    virtual RoutingIDPtr append(const RoutingIDPtr other) {
        const PrefixRoutingID &_other = *static_cast<const PrefixRoutingID *>(other.get());
        PrefixRoutingID *result = new PrefixRoutingID(*this);

        for (unsigned i = 0; i < _other.prefix.size(); ++i)
            result->prefix.push_back(_other.prefix[i]);

        return RoutingIDPtr(result);
    }

    virtual size_t hash() const {
        const size_t prime = 17;
        size_t result = 1;
        for (unsigned i = 0; i < prefix.size(); i++)
            result = prime * result + prefix[i];
        return result;
    }

    virtual std::string toString() const {
        std::string result = "[";
        for (unsigned i = 0; i < prefix.size(); i++) {
            if (i != 0)
                result.append(", ");
            char tmp[10];
            sprintf(tmp, "%d", prefix[i]);
            result.append(tmp);
        }
        result.append("]");
        return result;
    }
};



#endif /* PREFIXROUTINGID_H_ */
