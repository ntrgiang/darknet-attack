//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//

#ifndef HELLOINFO_H_
#define HELLOINFO_H_

#include "IDs.h"
#include <omnetpp.h>
#include <vector>

using namespace std;

class Age {
    simtime_t initialAge;
    simtime_t timeOnSet;

public:
    simtime_t maxAge;

    void set(simtime_t newAge) {
        initialAge = newAge;
        timeOnSet = simTime();
    }

    simtime_t get() {
        return initialAge + simTime() - timeOnSet;
    }

    bool isOutdated() {
        return get() > maxAge;
    }
};


class HelloInfo {
public:
    TreeNodeIDPtr sender;
    TreeNodeIDPtr root;
    int rootDist;
    Age age;

    RoutingIDPtr routingID;
    RoutingIDPtr offset;

    bool parentRequest;

    bool noMoreChildren;

    unsigned long updateID;

    int treeID;

    RoutingIDPtr currentlyUsedRoutingID;

};


//a container for packing several helloInfos together and putting them into a hello message
//(adding a vector to an OMNET msg file is problematic, but using this mini wrapper is not)
class HelloInfoContainer {
public:

    vector<HelloInfo> infos;

    void add(const HelloInfo &hello) {
        infos.push_back(hello);
    }

    unsigned size() {
        return infos.size();
    }

    HelloInfo &operator[](int index) {
        return infos[index];
    }

};














#endif /* HELLOINFO_H_ */
