//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//



#include "Statistic.h"
#include <iomanip> // setprecision
#include "Monitor.h"
#include <algorithm>
#include <vector>

using namespace std;

Define_Module(Statistic);

#ifndef debugOUT
#define debugOUT (!m_debug) ? std::cout : std::cout \
    << "@" << setprecision(12) \
    << simTime().dbl() << " @DnStatistic "
#endif


Statistic::Statistic() {
    // TODO Auto-generated constructor stub
}

Statistic::~Statistic() {
    // TODO Auto-generated destructor stub
}

void Statistic::finish()
{
    //cout << "success = " << Monitor::instance.success << endl;
    //cout << "success = " << Monitor::instance.failure << endl;


    long total = Monitor::instance.success + Monitor::instance.failure;
    assert(Monitor::instance.dropByChurn <= total);
    double successRate = (total == 0) ? 0.0 : (double)Monitor::instance.success / total;
    double failureRate = (total == 0) ? 0.0 : (double)Monitor::instance.failure / total;

    emit (sig_successRate, successRate);
    emit (sig_failureRate, failureRate);
    emit (sig_numSuccess, Monitor::instance.success);
    emit (sig_numFailure, Monitor::instance.failure);
    emit (sig_numLookup, total);

    emit (sig_noCloserNeighborFailure,  Monitor::instance.noCloserNeighborFailure);
    emit (sig_noNextHopFailure,         Monitor::instance.noNextHopFailure);
    emit (sig_badResponseFailure,       Monitor::instance.badResponseFailure);


    // --------------------------------------------------------
    // -- ID LOOKUP
    // --------------------------------------------------------

    total = Monitor::instance.idLookup;
    long failure = total - Monitor::instance.idLookupSuccess;
    emit (sig_numFailureFromStart,  Monitor::instance.getnumFailureFromStart());
    emit (sig_numIdLookupSuccess,   Monitor::instance.idLookupSuccess);
    emit (sig_numIdLookup,          total);
    emit (sig_numIdLookupFailure,   failure);
    emit (sig_numDropByChurn,       Monitor::instance.dropByChurn);

    successRate = (total == 0) ? 0.0 : (double) Monitor::instance.idLookupSuccess / total;
    failureRate = (total == 0) ? 0.0 : (double) failure / total;
    emit (sig_rateIdLookupSuccess, successRate);
    emit (sig_rateIdLookupFailure, failureRate);

    double rateDropByChurn = (total == 0) ? 0.0 : (double) Monitor::instance.dropByChurn / total;
    emit (sig_rateDropByChurn, rateDropByChurn);

    double rate = (total == 0) ? 0.0 : (double) Monitor::instance.incorrectIdRouting / total;
    emit (sig_numIncorrectIdRouting, Monitor::instance.incorrectIdRouting);
    emit (sig_rateIncorrectIdRouting, rate);
    //Monitor::instance.printRoutingIdList();

    for (vector<double>::iterator it = Monitor::instance.m_joinTimes.begin();
         it != Monitor::instance.m_joinTimes.end(); ++it) {
        emit (sig_joinTimes, *it);
    }

    // -- Hop count
    for (vector<int>::iterator it = Monitor::instance.m_hopCounts.begin();
         it != Monitor::instance.m_hopCounts.end(); ++it) {
        emit (sig_hopCount, *it);
    }

    // -- Root change
    for (map<int, double>::iterator it = Monitor::instance.m_rootChanges.begin();
         it != Monitor::instance.m_rootChanges.end(); ++it) {
        emit (sig_rootChange, it->second);
    }

    emit (sig_numSameComponent, Monitor::instance.getNumSameComponent());
    emit (sig_numDiffComponent, Monitor::instance.getNumDiffComponent());


    // ------------------------------------
    // ------------------------------------
    // -- Debugging
    // ------------------------------------
    // ------------------------------------

    Monitor::instance.printLookupMessages();
    Monitor::instance.printIncorrectRoutingMessages();
    Monitor::instance.printSuccessMessages();

    vector<long> lookupMessages = Monitor::instance.getLookupMessages();
    vector<long> successMessages = Monitor::instance.getSuccessMessages();

    std::sort(lookupMessages.begin(), lookupMessages.end());
    std::sort(successMessages.begin(), successMessages.end());

    vector<long>::iterator iter;
    vector<long> failureMessages;
    failureMessages.resize(lookupMessages.size());
    iter = std::set_difference(lookupMessages.begin(), lookupMessages.end(),
                               successMessages.begin(), successMessages.end(),
                               failureMessages.begin());
    failureMessages.resize(iter-failureMessages.begin());
    cout << "lookup messages " << lookupMessages.size() << endl;
    cout << "failure messages " << failureMessages.size() << endl;
    cout << "incorrect messages: " << Monitor::instance.getIncorrectRoutingMessages().size() << endl;



}

void Statistic::initialize(int stage)
{
    if (stage == 0)
    {
        m_debug = par("debug");

        // -- Storage Lookup
        sig_successRate = registerSignal("Signal_successRate");
        sig_failureRate = registerSignal("Signal_failureRate");
        sig_numSuccess  = registerSignal("Signal_numSuccess");
        sig_numFailure  = registerSignal("Signal_numFailure");
        sig_numLookup   = registerSignal("Signal_numLookup");

        // -- ID LOOKUP
        sig_numSameComponent = registerSignal("Signal_numSameComponent");
        sig_numDiffComponent = registerSignal("Signal_numDiffComponent");

        sig_noCloserNeighborFailure = registerSignal("Signal_NoCloserNeighborFailure");
        sig_noNextHopFailure        = registerSignal("Signal_NoNextHopFailure");
        sig_badResponseFailure      = registerSignal("Signal_BadResponseFailure");

        sig_numFailureFromStart     = registerSignal("Signal_numFailureFromStart");
        sig_numIdLookup             = registerSignal("Signal_numIdLookup");
        sig_numIdLookupSuccess      = registerSignal("Signal_numIdLookupSuccess");
        sig_numIdLookupFailure      = registerSignal("Signal_numIdLookupFailure");
        sig_numIncorrectIdRouting   = registerSignal("Signal_numIncorrectIdRouting");
        sig_numDropByChurn          = registerSignal("Signal_numDropByChurn");

        sig_rateIdLookupSuccess     = registerSignal("Signal_rateIdLookupSuccess");
        sig_rateIdLookupFailure     = registerSignal("Signal_rateIdLookupFailure");
        sig_rateIncorrectIdRouting  = registerSignal("Signal_rateIncorrectIdRouting");
        sig_rateDropByChurn         = registerSignal("Signal_rateDropByChurn");

        sig_joinTimes   = registerSignal("Signal_JoinTimes");
        sig_hopCount    = registerSignal("Signal_HopCount");
        sig_rootChange  = registerSignal("Signal_RootChange");
    }

    if (stage != 3)
        return;
}

void Statistic::handleMessage(cMessage *msg)
{
    if (msg->isSelfMessage())
    {
        handleTimerMessage(msg);
    }
    else
    {
        throw cException("ActivePeerTable doesn't process messages!");
    }
}

void Statistic::handleTimerMessage(cMessage *msg)
{
    debugOUT << "--handleTimerMessage--" << endl;
}


