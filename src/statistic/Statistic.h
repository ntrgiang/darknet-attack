//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//



#ifndef STATISTIC_H_
#define STATISTIC_H_

#include "omnetpp.h"

class Statistic : public cSimpleModule
{
public:
   Statistic();
   virtual ~Statistic();

   virtual int numInitStages() const  {return 4;}
   virtual void initialize(int stage);

   virtual void handleMessage(cMessage *msg);
   virtual void handleTimerMessage(cMessage *msg);
   virtual void finish();

protected:
   bool m_debug;

   // -- Storage Lookup
   simsignal_t sig_successRate;
   simsignal_t sig_failureRate;
   simsignal_t sig_numSuccess;
   simsignal_t sig_numFailure;
   simsignal_t sig_numLookup;

   // -- ID LOOKUP
   simsignal_t sig_numSameComponent;
   simsignal_t sig_numDiffComponent;

   simsignal_t sig_noCloserNeighborFailure;
   simsignal_t sig_noNextHopFailure;
   simsignal_t sig_badResponseFailure;

   simsignal_t sig_numFailureFromStart;
   simsignal_t sig_numIdLookup;
   simsignal_t sig_numIdLookupSuccess;
   simsignal_t sig_numIdLookupFailure;
   simsignal_t sig_numIncorrectIdRouting;
   simsignal_t sig_numDropByChurn;

   simsignal_t sig_rateIdLookupSuccess;
   simsignal_t sig_rateIdLookupFailure;
   simsignal_t sig_rateIncorrectIdRouting;
   simsignal_t sig_rateDropByChurn;

   simsignal_t sig_joinTimes;
   simsignal_t sig_hopCount;

   simsignal_t sig_rootChange;

};

#endif /* STATISTIC_H_ */
