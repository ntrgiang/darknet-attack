#!/usr/bin/python3

import random
import sys

rnd = random.Random()

if len(sys.argv) < 4:
	print("usage: python createSmallWorldGraph.py n k beta")
	print("n = number of nodes")
	print("k = edges created per node")
	print("(so in the end each node will have around 2k edges)")
	print("beta = probability that an edge is redirected to a random node")
	exit()

n = int(sys.argv[1]) #number of nodes
k = int(sys.argv[2]) #avg. number of edges
beta = float(sys.argv[3]) # prob for edge redirection

graph = []

#create initial graph
for i in range(n):
	graph.append([])
	for j in range(i + 1, i + k + 1):
		graph[i].append(j % n)
		
#redirect edges
for (node, edges) in enumerate(graph):
	for i in range(len(edges)):
		oldNeighbor = edges[i]
		if rnd.random() < beta:
			while True:
				newNeighbor = rnd.randrange(n)
				if newNeighbor != node and not newNeighbor in edges and not node in graph[newNeighbor]:
					edges[i] = newNeighbor
					break

#add backward edges
for (node, edges) in enumerate(graph):
	for (node2, edges2) in enumerate(graph):
		if node != node2:
			if node in edges2 and not node2 in edges:
				edges.append(node2)
					
#output graph
for (node, edges) in enumerate(graph):
	print(str(node) + ":", end="")
	for (i, e) in enumerate(edges):
		if i != 0:
			print(";", end="")
		print(e, end="")
	print()
		